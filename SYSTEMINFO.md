# Cisco Firepower Threat Defense 

Vendor: Cisco Systems 
Homepage: https://www.cisco.com/

Product: Firepower Threat Defense 
Product Page: https://www.cisco.com/c/dam/en/us/products/security/firewalls/cisco-ftd-policy-management-common-practices.pdf

## Introduction
We classify Cisco Firepower Threat Defense into the Security (SASE) domain as Cisco Firepower Threat Defense provides Security solutions. 

"Cisco Firepower Threat Defense (FTD) policies help you flag specific network traffic patterns, create alerts and better control your network." 

## Why Integrate
The Cisco Firepower Threat Defense adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Cisco Firepower Threat Defense to offer security and trust information. 

With this adapter you have the ability to perform operations with Cisco Firepower Threat Defense such as:

- Objects
- Policies
- Devices
- DNS
- Jobs

## Additional Product Documentation
The [Cisco Systems - Firepower Threat Defense API](https://www.cisco.com/c/en/us/td/docs/security/firepower/ftd-api/guide/ftd-rest-api.html)
