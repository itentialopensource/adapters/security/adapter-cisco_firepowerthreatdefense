/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-cisco_firepowerthreatdefense',
      type: 'FirepowerThreatDefense',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const FirepowerThreatDefense = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Firepower_threat_defense Adapter Test', () => {
  describe('FirepowerThreatDefense Class Tests', () => {
    const a = new FirepowerThreatDefense(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('cisco_firepowerthreatdefense'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('cisco_firepowerthreatdefense'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('FirepowerThreatDefense', pronghornDotJson.export);
          assert.equal('Firepower_threat_defense', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-cisco_firepowerthreatdefense', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('cisco_firepowerthreatdefense'));
          assert.equal('FirepowerThreatDefense', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-cisco_firepowerthreatdefense', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-cisco_firepowerthreatdefense', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#getInterfaceMigrationImmediateList - errors', () => {
      it('should have a getInterfaceMigrationImmediateList function', (done) => {
        try {
          assert.equal(true, typeof a.getInterfaceMigrationImmediateList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addInterfaceMigrationImmediate - errors', () => {
      it('should have a addInterfaceMigrationImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.addInterfaceMigrationImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addInterfaceMigrationImmediate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addInterfaceMigrationImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryInterfaceMigrationList - errors', () => {
      it('should have a getJobHistoryInterfaceMigrationList function', (done) => {
        try {
          assert.equal(true, typeof a.getJobHistoryInterfaceMigrationList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryInterfaceMigration - errors', () => {
      it('should have a getJobHistoryInterfaceMigration function', (done) => {
        try {
          assert.equal(true, typeof a.getJobHistoryInterfaceMigration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getJobHistoryInterfaceMigration(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getJobHistoryInterfaceMigration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJobHistoryInterfaceMigration - errors', () => {
      it('should have a deleteJobHistoryInterfaceMigration function', (done) => {
        try {
          assert.equal(true, typeof a.deleteJobHistoryInterfaceMigration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteJobHistoryInterfaceMigration(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteJobHistoryInterfaceMigration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPhysicalInterfaceList - errors', () => {
      it('should have a getPhysicalInterfaceList function', (done) => {
        try {
          assert.equal(true, typeof a.getPhysicalInterfaceList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPhysicalInterface - errors', () => {
      it('should have a getPhysicalInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getPhysicalInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getPhysicalInterface(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getPhysicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editPhysicalInterface - errors', () => {
      it('should have a editPhysicalInterface function', (done) => {
        try {
          assert.equal(true, typeof a.editPhysicalInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editPhysicalInterface(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editPhysicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editPhysicalInterface('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editPhysicalInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBridgeGroupInterfaceList - errors', () => {
      it('should have a getBridgeGroupInterfaceList function', (done) => {
        try {
          assert.equal(true, typeof a.getBridgeGroupInterfaceList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addBridgeGroupInterface - errors', () => {
      it('should have a addBridgeGroupInterface function', (done) => {
        try {
          assert.equal(true, typeof a.addBridgeGroupInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addBridgeGroupInterface(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addBridgeGroupInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBridgeGroupInterface - errors', () => {
      it('should have a getBridgeGroupInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getBridgeGroupInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getBridgeGroupInterface(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getBridgeGroupInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editBridgeGroupInterface - errors', () => {
      it('should have a editBridgeGroupInterface function', (done) => {
        try {
          assert.equal(true, typeof a.editBridgeGroupInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editBridgeGroupInterface(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editBridgeGroupInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editBridgeGroupInterface('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editBridgeGroupInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBridgeGroupInterface - errors', () => {
      it('should have a deleteBridgeGroupInterface function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBridgeGroupInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteBridgeGroupInterface(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteBridgeGroupInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSubInterfaceList - errors', () => {
      it('should have a getSubInterfaceList function', (done) => {
        try {
          assert.equal(true, typeof a.getSubInterfaceList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.getSubInterfaceList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSubInterfaceList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSubInterface - errors', () => {
      it('should have a addSubInterface function', (done) => {
        try {
          assert.equal(true, typeof a.addSubInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.addSubInterface('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSubInterface('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSubInterface - errors', () => {
      it('should have a getSubInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getSubInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.getSubInterface(null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getSubInterface('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editSubInterface - errors', () => {
      it('should have a editSubInterface function', (done) => {
        try {
          assert.equal(true, typeof a.editSubInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.editSubInterface('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editSubInterface('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editSubInterface('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSubInterface - errors', () => {
      it('should have a deleteSubInterface function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSubInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.deleteSubInterface(null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteSubInterface('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVlanInterfaceList - errors', () => {
      it('should have a getVlanInterfaceList function', (done) => {
        try {
          assert.equal(true, typeof a.getVlanInterfaceList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addVlanInterface - errors', () => {
      it('should have a addVlanInterface function', (done) => {
        try {
          assert.equal(true, typeof a.addVlanInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addVlanInterface(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addVlanInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVlanInterface - errors', () => {
      it('should have a getVlanInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getVlanInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getVlanInterface(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getVlanInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editVlanInterface - errors', () => {
      it('should have a editVlanInterface function', (done) => {
        try {
          assert.equal(true, typeof a.editVlanInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editVlanInterface(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editVlanInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editVlanInterface('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editVlanInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVlanInterface - errors', () => {
      it('should have a deleteVlanInterface function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVlanInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteVlanInterface(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteVlanInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEtherChannelInterfaceList - errors', () => {
      it('should have a getEtherChannelInterfaceList function', (done) => {
        try {
          assert.equal(true, typeof a.getEtherChannelInterfaceList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addEtherChannelInterface - errors', () => {
      it('should have a addEtherChannelInterface function', (done) => {
        try {
          assert.equal(true, typeof a.addEtherChannelInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addEtherChannelInterface(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addEtherChannelInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEtherChannelInterface - errors', () => {
      it('should have a getEtherChannelInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getEtherChannelInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getEtherChannelInterface(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getEtherChannelInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editEtherChannelInterface - errors', () => {
      it('should have a editEtherChannelInterface function', (done) => {
        try {
          assert.equal(true, typeof a.editEtherChannelInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editEtherChannelInterface(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editEtherChannelInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editEtherChannelInterface('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editEtherChannelInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEtherChannelInterface - errors', () => {
      it('should have a deleteEtherChannelInterface function', (done) => {
        try {
          assert.equal(true, typeof a.deleteEtherChannelInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteEtherChannelInterface(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteEtherChannelInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEtherChannelSubInterfaceList - errors', () => {
      it('should have a getEtherChannelSubInterfaceList function', (done) => {
        try {
          assert.equal(true, typeof a.getEtherChannelSubInterfaceList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.getEtherChannelSubInterfaceList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getEtherChannelSubInterfaceList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addEtherChannelSubInterface - errors', () => {
      it('should have a addEtherChannelSubInterface function', (done) => {
        try {
          assert.equal(true, typeof a.addEtherChannelSubInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.addEtherChannelSubInterface('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addEtherChannelSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addEtherChannelSubInterface('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addEtherChannelSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEtherChannelSubInterface - errors', () => {
      it('should have a getEtherChannelSubInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getEtherChannelSubInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.getEtherChannelSubInterface(null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getEtherChannelSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getEtherChannelSubInterface('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getEtherChannelSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editEtherChannelSubInterface - errors', () => {
      it('should have a editEtherChannelSubInterface function', (done) => {
        try {
          assert.equal(true, typeof a.editEtherChannelSubInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.editEtherChannelSubInterface('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editEtherChannelSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editEtherChannelSubInterface('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editEtherChannelSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editEtherChannelSubInterface('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editEtherChannelSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEtherChannelSubInterface - errors', () => {
      it('should have a deleteEtherChannelSubInterface function', (done) => {
        try {
          assert.equal(true, typeof a.deleteEtherChannelSubInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.deleteEtherChannelSubInterface(null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteEtherChannelSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteEtherChannelSubInterface('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteEtherChannelSubInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaceDataList - errors', () => {
      it('should have a getInterfaceDataList function', (done) => {
        try {
          assert.equal(true, typeof a.getInterfaceDataList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaceData - errors', () => {
      it('should have a getInterfaceData function', (done) => {
        try {
          assert.equal(true, typeof a.getInterfaceData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getInterfaceData(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getInterfaceData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemInformation - errors', () => {
      it('should have a getSystemInformation function', (done) => {
        try {
          assert.equal(true, typeof a.getSystemInformation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getSystemInformation(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSystemInformation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUrlCategoryInfo - errors', () => {
      it('should have a getUrlCategoryInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getUrlCategoryInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getUrlCategoryInfo(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getUrlCategoryInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addCommand - errors', () => {
      it('should have a addCommand function', (done) => {
        try {
          assert.equal(true, typeof a.addCommand === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addCommand(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addCommand', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCommandAutoComplete - errors', () => {
      it('should have a getCommandAutoComplete function', (done) => {
        try {
          assert.equal(true, typeof a.getCommandAutoComplete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getCommandAutoComplete(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getCommandAutoComplete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addInterfacePresenceChange - errors', () => {
      it('should have a addInterfacePresenceChange function', (done) => {
        try {
          assert.equal(true, typeof a.addInterfacePresenceChange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addInterfacePresenceChange(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addInterfacePresenceChange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFeatureInformation - errors', () => {
      it('should have a getFeatureInformation function', (done) => {
        try {
          assert.equal(true, typeof a.getFeatureInformation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getFeatureInformation(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getFeatureInformation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTelemetry - errors', () => {
      it('should have a getTelemetry function', (done) => {
        try {
          assert.equal(true, typeof a.getTelemetry === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getTelemetry(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getTelemetry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHTTPAccessListList - errors', () => {
      it('should have a getHTTPAccessListList function', (done) => {
        try {
          assert.equal(true, typeof a.getHTTPAccessListList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHTTPAccessList - errors', () => {
      it('should have a getHTTPAccessList function', (done) => {
        try {
          assert.equal(true, typeof a.getHTTPAccessList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getHTTPAccessList(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getHTTPAccessList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editHTTPAccessList - errors', () => {
      it('should have a editHTTPAccessList function', (done) => {
        try {
          assert.equal(true, typeof a.editHTTPAccessList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editHTTPAccessList(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editHTTPAccessList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editHTTPAccessList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editHTTPAccessList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSSHAccessListList - errors', () => {
      it('should have a getSSHAccessListList function', (done) => {
        try {
          assert.equal(true, typeof a.getSSHAccessListList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSSHAccessList - errors', () => {
      it('should have a getSSHAccessList function', (done) => {
        try {
          assert.equal(true, typeof a.getSSHAccessList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getSSHAccessList(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSSHAccessList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editSSHAccessList - errors', () => {
      it('should have a editSSHAccessList function', (done) => {
        try {
          assert.equal(true, typeof a.editSSHAccessList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editSSHAccessList(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSSHAccessList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editSSHAccessList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSSHAccessList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDataInterfaceManagementAccessList - errors', () => {
      it('should have a getDataInterfaceManagementAccessList function', (done) => {
        try {
          assert.equal(true, typeof a.getDataInterfaceManagementAccessList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addDataInterfaceManagementAccess - errors', () => {
      it('should have a addDataInterfaceManagementAccess function', (done) => {
        try {
          assert.equal(true, typeof a.addDataInterfaceManagementAccess === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addDataInterfaceManagementAccess(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addDataInterfaceManagementAccess', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDataInterfaceManagementAccess - errors', () => {
      it('should have a getDataInterfaceManagementAccess function', (done) => {
        try {
          assert.equal(true, typeof a.getDataInterfaceManagementAccess === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getDataInterfaceManagementAccess(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getDataInterfaceManagementAccess', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editDataInterfaceManagementAccess - errors', () => {
      it('should have a editDataInterfaceManagementAccess function', (done) => {
        try {
          assert.equal(true, typeof a.editDataInterfaceManagementAccess === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editDataInterfaceManagementAccess(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editDataInterfaceManagementAccess', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editDataInterfaceManagementAccess('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editDataInterfaceManagementAccess', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDataInterfaceManagementAccess - errors', () => {
      it('should have a deleteDataInterfaceManagementAccess function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDataInterfaceManagementAccess === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteDataInterfaceManagementAccess(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteDataInterfaceManagementAccess', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceHostnameList - errors', () => {
      it('should have a getDeviceHostnameList function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceHostnameList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceHostname - errors', () => {
      it('should have a getDeviceHostname function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceHostname === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getDeviceHostname(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getDeviceHostname', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editDeviceHostname - errors', () => {
      it('should have a editDeviceHostname function', (done) => {
        try {
          assert.equal(true, typeof a.editDeviceHostname === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editDeviceHostname(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editDeviceHostname', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editDeviceHostname('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editDeviceHostname', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebUICertificateList - errors', () => {
      it('should have a getWebUICertificateList function', (done) => {
        try {
          assert.equal(true, typeof a.getWebUICertificateList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebUICertificate - errors', () => {
      it('should have a getWebUICertificate function', (done) => {
        try {
          assert.equal(true, typeof a.getWebUICertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getWebUICertificate(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getWebUICertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editWebUICertificate - errors', () => {
      it('should have a editWebUICertificate function', (done) => {
        try {
          assert.equal(true, typeof a.editWebUICertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editWebUICertificate(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editWebUICertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editWebUICertificate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editWebUICertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getManagementIPList - errors', () => {
      it('should have a getManagementIPList function', (done) => {
        try {
          assert.equal(true, typeof a.getManagementIPList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getManagementIP - errors', () => {
      it('should have a getManagementIP function', (done) => {
        try {
          assert.equal(true, typeof a.getManagementIP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getManagementIP(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getManagementIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editManagementIP - errors', () => {
      it('should have a editManagementIP function', (done) => {
        try {
          assert.equal(true, typeof a.editManagementIP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editManagementIP(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editManagementIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editManagementIP('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editManagementIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityZoneList - errors', () => {
      it('should have a getSecurityZoneList function', (done) => {
        try {
          assert.equal(true, typeof a.getSecurityZoneList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSecurityZone - errors', () => {
      it('should have a addSecurityZone function', (done) => {
        try {
          assert.equal(true, typeof a.addSecurityZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSecurityZone(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addSecurityZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityZone - errors', () => {
      it('should have a getSecurityZone function', (done) => {
        try {
          assert.equal(true, typeof a.getSecurityZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getSecurityZone(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSecurityZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editSecurityZone - errors', () => {
      it('should have a editSecurityZone function', (done) => {
        try {
          assert.equal(true, typeof a.editSecurityZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editSecurityZone(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSecurityZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editSecurityZone('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSecurityZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecurityZone - errors', () => {
      it('should have a deleteSecurityZone function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecurityZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteSecurityZone(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteSecurityZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityServicesEngineList - errors', () => {
      it('should have a getIdentityServicesEngineList function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityServicesEngineList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addIdentityServicesEngine - errors', () => {
      it('should have a addIdentityServicesEngine function', (done) => {
        try {
          assert.equal(true, typeof a.addIdentityServicesEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addIdentityServicesEngine(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addIdentityServicesEngine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityServicesEngine - errors', () => {
      it('should have a getIdentityServicesEngine function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityServicesEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getIdentityServicesEngine(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getIdentityServicesEngine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editIdentityServicesEngine - errors', () => {
      it('should have a editIdentityServicesEngine function', (done) => {
        try {
          assert.equal(true, typeof a.editIdentityServicesEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editIdentityServicesEngine(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editIdentityServicesEngine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editIdentityServicesEngine('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editIdentityServicesEngine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIdentityServicesEngine - errors', () => {
      it('should have a deleteIdentityServicesEngine function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIdentityServicesEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteIdentityServicesEngine(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteIdentityServicesEngine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStandardAccessListList - errors', () => {
      it('should have a getStandardAccessListList function', (done) => {
        try {
          assert.equal(true, typeof a.getStandardAccessListList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addStandardAccessList - errors', () => {
      it('should have a addStandardAccessList function', (done) => {
        try {
          assert.equal(true, typeof a.addStandardAccessList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addStandardAccessList(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addStandardAccessList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStandardAccessList - errors', () => {
      it('should have a getStandardAccessList function', (done) => {
        try {
          assert.equal(true, typeof a.getStandardAccessList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getStandardAccessList(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getStandardAccessList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editStandardAccessList - errors', () => {
      it('should have a editStandardAccessList function', (done) => {
        try {
          assert.equal(true, typeof a.editStandardAccessList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editStandardAccessList(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editStandardAccessList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editStandardAccessList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editStandardAccessList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteStandardAccessList - errors', () => {
      it('should have a deleteStandardAccessList function', (done) => {
        try {
          assert.equal(true, typeof a.deleteStandardAccessList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteStandardAccessList(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteStandardAccessList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtendedAccessListList - errors', () => {
      it('should have a getExtendedAccessListList function', (done) => {
        try {
          assert.equal(true, typeof a.getExtendedAccessListList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addExtendedAccessList - errors', () => {
      it('should have a addExtendedAccessList function', (done) => {
        try {
          assert.equal(true, typeof a.addExtendedAccessList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addExtendedAccessList(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addExtendedAccessList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtendedAccessList - errors', () => {
      it('should have a getExtendedAccessList function', (done) => {
        try {
          assert.equal(true, typeof a.getExtendedAccessList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getExtendedAccessList(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getExtendedAccessList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editExtendedAccessList - errors', () => {
      it('should have a editExtendedAccessList function', (done) => {
        try {
          assert.equal(true, typeof a.editExtendedAccessList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editExtendedAccessList(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editExtendedAccessList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editExtendedAccessList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editExtendedAccessList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtendedAccessList - errors', () => {
      it('should have a deleteExtendedAccessList function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtendedAccessList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteExtendedAccessList(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteExtendedAccessList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getASPathListList - errors', () => {
      it('should have a getASPathListList function', (done) => {
        try {
          assert.equal(true, typeof a.getASPathListList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addASPathList - errors', () => {
      it('should have a addASPathList function', (done) => {
        try {
          assert.equal(true, typeof a.addASPathList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addASPathList(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addASPathList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getASPathList - errors', () => {
      it('should have a getASPathList function', (done) => {
        try {
          assert.equal(true, typeof a.getASPathList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getASPathList(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getASPathList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editASPathList - errors', () => {
      it('should have a editASPathList function', (done) => {
        try {
          assert.equal(true, typeof a.editASPathList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editASPathList(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editASPathList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editASPathList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editASPathList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteASPathList - errors', () => {
      it('should have a deleteASPathList function', (done) => {
        try {
          assert.equal(true, typeof a.deleteASPathList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteASPathList(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteASPathList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRouteMapList - errors', () => {
      it('should have a getRouteMapList function', (done) => {
        try {
          assert.equal(true, typeof a.getRouteMapList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addRouteMap - errors', () => {
      it('should have a addRouteMap function', (done) => {
        try {
          assert.equal(true, typeof a.addRouteMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addRouteMap(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addRouteMap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRouteMap - errors', () => {
      it('should have a getRouteMap function', (done) => {
        try {
          assert.equal(true, typeof a.getRouteMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getRouteMap(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getRouteMap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editRouteMap - errors', () => {
      it('should have a editRouteMap function', (done) => {
        try {
          assert.equal(true, typeof a.editRouteMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editRouteMap(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editRouteMap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editRouteMap('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editRouteMap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRouteMap - errors', () => {
      it('should have a deleteRouteMap function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRouteMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteRouteMap(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteRouteMap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStandardCommunityListList - errors', () => {
      it('should have a getStandardCommunityListList function', (done) => {
        try {
          assert.equal(true, typeof a.getStandardCommunityListList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addStandardCommunityList - errors', () => {
      it('should have a addStandardCommunityList function', (done) => {
        try {
          assert.equal(true, typeof a.addStandardCommunityList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addStandardCommunityList(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addStandardCommunityList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStandardCommunityList - errors', () => {
      it('should have a getStandardCommunityList function', (done) => {
        try {
          assert.equal(true, typeof a.getStandardCommunityList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getStandardCommunityList(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getStandardCommunityList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editStandardCommunityList - errors', () => {
      it('should have a editStandardCommunityList function', (done) => {
        try {
          assert.equal(true, typeof a.editStandardCommunityList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editStandardCommunityList(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editStandardCommunityList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editStandardCommunityList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editStandardCommunityList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteStandardCommunityList - errors', () => {
      it('should have a deleteStandardCommunityList function', (done) => {
        try {
          assert.equal(true, typeof a.deleteStandardCommunityList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteStandardCommunityList(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteStandardCommunityList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExpandedCommunityListList - errors', () => {
      it('should have a getExpandedCommunityListList function', (done) => {
        try {
          assert.equal(true, typeof a.getExpandedCommunityListList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addExpandedCommunityList - errors', () => {
      it('should have a addExpandedCommunityList function', (done) => {
        try {
          assert.equal(true, typeof a.addExpandedCommunityList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addExpandedCommunityList(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addExpandedCommunityList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExpandedCommunityList - errors', () => {
      it('should have a getExpandedCommunityList function', (done) => {
        try {
          assert.equal(true, typeof a.getExpandedCommunityList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getExpandedCommunityList(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getExpandedCommunityList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editExpandedCommunityList - errors', () => {
      it('should have a editExpandedCommunityList function', (done) => {
        try {
          assert.equal(true, typeof a.editExpandedCommunityList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editExpandedCommunityList(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editExpandedCommunityList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editExpandedCommunityList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editExpandedCommunityList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExpandedCommunityList - errors', () => {
      it('should have a deleteExpandedCommunityList function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExpandedCommunityList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteExpandedCommunityList(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteExpandedCommunityList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPV4PrefixListList - errors', () => {
      it('should have a getIPV4PrefixListList function', (done) => {
        try {
          assert.equal(true, typeof a.getIPV4PrefixListList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addIPV4PrefixList - errors', () => {
      it('should have a addIPV4PrefixList function', (done) => {
        try {
          assert.equal(true, typeof a.addIPV4PrefixList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addIPV4PrefixList(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addIPV4PrefixList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPV4PrefixList - errors', () => {
      it('should have a getIPV4PrefixList function', (done) => {
        try {
          assert.equal(true, typeof a.getIPV4PrefixList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getIPV4PrefixList(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getIPV4PrefixList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editIPV4PrefixList - errors', () => {
      it('should have a editIPV4PrefixList function', (done) => {
        try {
          assert.equal(true, typeof a.editIPV4PrefixList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editIPV4PrefixList(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editIPV4PrefixList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editIPV4PrefixList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editIPV4PrefixList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIPV4PrefixList - errors', () => {
      it('should have a deleteIPV4PrefixList function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIPV4PrefixList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteIPV4PrefixList(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteIPV4PrefixList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPV6PrefixListList - errors', () => {
      it('should have a getIPV6PrefixListList function', (done) => {
        try {
          assert.equal(true, typeof a.getIPV6PrefixListList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addIPV6PrefixList - errors', () => {
      it('should have a addIPV6PrefixList function', (done) => {
        try {
          assert.equal(true, typeof a.addIPV6PrefixList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addIPV6PrefixList(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addIPV6PrefixList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPV6PrefixList - errors', () => {
      it('should have a getIPV6PrefixList function', (done) => {
        try {
          assert.equal(true, typeof a.getIPV6PrefixList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getIPV6PrefixList(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getIPV6PrefixList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editIPV6PrefixList - errors', () => {
      it('should have a editIPV6PrefixList function', (done) => {
        try {
          assert.equal(true, typeof a.editIPV6PrefixList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editIPV6PrefixList(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editIPV6PrefixList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editIPV6PrefixList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editIPV6PrefixList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIPV6PrefixList - errors', () => {
      it('should have a deleteIPV6PrefixList function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIPV6PrefixList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteIPV6PrefixList(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteIPV6PrefixList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyListList - errors', () => {
      it('should have a getPolicyListList function', (done) => {
        try {
          assert.equal(true, typeof a.getPolicyListList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPolicyList - errors', () => {
      it('should have a addPolicyList function', (done) => {
        try {
          assert.equal(true, typeof a.addPolicyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addPolicyList(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addPolicyList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyList - errors', () => {
      it('should have a getPolicyList function', (done) => {
        try {
          assert.equal(true, typeof a.getPolicyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getPolicyList(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getPolicyList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editPolicyList - errors', () => {
      it('should have a editPolicyList function', (done) => {
        try {
          assert.equal(true, typeof a.editPolicyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editPolicyList(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editPolicyList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editPolicyList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editPolicyList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicyList - errors', () => {
      it('should have a deletePolicyList function', (done) => {
        try {
          assert.equal(true, typeof a.deletePolicyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deletePolicyList(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deletePolicyList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHardwareBypassList - errors', () => {
      it('should have a getHardwareBypassList function', (done) => {
        try {
          assert.equal(true, typeof a.getHardwareBypassList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHardwareBypass - errors', () => {
      it('should have a getHardwareBypass function', (done) => {
        try {
          assert.equal(true, typeof a.getHardwareBypass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getHardwareBypass(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getHardwareBypass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editHardwareBypass - errors', () => {
      it('should have a editHardwareBypass function', (done) => {
        try {
          assert.equal(true, typeof a.editHardwareBypass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editHardwareBypass(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editHardwareBypass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editHardwareBypass('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editHardwareBypass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationList - errors', () => {
      it('should have a getApplicationList function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationTagList - errors', () => {
      it('should have a getApplicationTagList function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationTagList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationCategoryList - errors', () => {
      it('should have a getApplicationCategoryList function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationCategoryList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationFilterList - errors', () => {
      it('should have a getApplicationFilterList function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationFilterList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addApplicationFilter - errors', () => {
      it('should have a addApplicationFilter function', (done) => {
        try {
          assert.equal(true, typeof a.addApplicationFilter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addApplicationFilter(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addApplicationFilter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationFilter - errors', () => {
      it('should have a getApplicationFilter function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationFilter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getApplicationFilter(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getApplicationFilter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editApplicationFilter - errors', () => {
      it('should have a editApplicationFilter function', (done) => {
        try {
          assert.equal(true, typeof a.editApplicationFilter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editApplicationFilter(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editApplicationFilter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editApplicationFilter('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editApplicationFilter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplicationFilter - errors', () => {
      it('should have a deleteApplicationFilter function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApplicationFilter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteApplicationFilter(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteApplicationFilter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCountryList - errors', () => {
      it('should have a getCountryList function', (done) => {
        try {
          assert.equal(true, typeof a.getCountryList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCountry - errors', () => {
      it('should have a getCountry function', (done) => {
        try {
          assert.equal(true, typeof a.getCountry === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getCountry(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getCountry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContinentList - errors', () => {
      it('should have a getContinentList function', (done) => {
        try {
          assert.equal(true, typeof a.getContinentList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContinent - errors', () => {
      it('should have a getContinent function', (done) => {
        try {
          assert.equal(true, typeof a.getContinent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getContinent(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getContinent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getURLCategoryList - errors', () => {
      it('should have a getURLCategoryList function', (done) => {
        try {
          assert.equal(true, typeof a.getURLCategoryList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getURLReputationList - errors', () => {
      it('should have a getURLReputationList function', (done) => {
        try {
          assert.equal(true, typeof a.getURLReputationList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGeoLocationList - errors', () => {
      it('should have a getGeoLocationList function', (done) => {
        try {
          assert.equal(true, typeof a.getGeoLocationList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addGeoLocation - errors', () => {
      it('should have a addGeoLocation function', (done) => {
        try {
          assert.equal(true, typeof a.addGeoLocation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addGeoLocation(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addGeoLocation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGeoLocation - errors', () => {
      it('should have a getGeoLocation function', (done) => {
        try {
          assert.equal(true, typeof a.getGeoLocation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getGeoLocation(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getGeoLocation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editGeoLocation - errors', () => {
      it('should have a editGeoLocation function', (done) => {
        try {
          assert.equal(true, typeof a.editGeoLocation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editGeoLocation(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editGeoLocation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editGeoLocation('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editGeoLocation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGeoLocation - errors', () => {
      it('should have a deleteGeoLocation function', (done) => {
        try {
          assert.equal(true, typeof a.deleteGeoLocation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteGeoLocation(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteGeoLocation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getURLObjectList - errors', () => {
      it('should have a getURLObjectList function', (done) => {
        try {
          assert.equal(true, typeof a.getURLObjectList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addURLObject - errors', () => {
      it('should have a addURLObject function', (done) => {
        try {
          assert.equal(true, typeof a.addURLObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addURLObject(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addURLObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getURLObject - errors', () => {
      it('should have a getURLObject function', (done) => {
        try {
          assert.equal(true, typeof a.getURLObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getURLObject(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getURLObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editURLObject - errors', () => {
      it('should have a editURLObject function', (done) => {
        try {
          assert.equal(true, typeof a.editURLObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editURLObject(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editURLObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editURLObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editURLObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteURLObject - errors', () => {
      it('should have a deleteURLObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteURLObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteURLObject(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteURLObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getURLObjectGroupList - errors', () => {
      it('should have a getURLObjectGroupList function', (done) => {
        try {
          assert.equal(true, typeof a.getURLObjectGroupList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addURLObjectGroup - errors', () => {
      it('should have a addURLObjectGroup function', (done) => {
        try {
          assert.equal(true, typeof a.addURLObjectGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addURLObjectGroup(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addURLObjectGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getURLObjectGroup - errors', () => {
      it('should have a getURLObjectGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getURLObjectGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getURLObjectGroup(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getURLObjectGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editURLObjectGroup - errors', () => {
      it('should have a editURLObjectGroup function', (done) => {
        try {
          assert.equal(true, typeof a.editURLObjectGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editURLObjectGroup(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editURLObjectGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editURLObjectGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editURLObjectGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteURLObjectGroup - errors', () => {
      it('should have a deleteURLObjectGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteURLObjectGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteURLObjectGroup(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteURLObjectGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeploymentList - errors', () => {
      it('should have a getDeploymentList function', (done) => {
        try {
          assert.equal(true, typeof a.getDeploymentList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addDeployment - errors', () => {
      it('should have a addDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.addDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeployment - errors', () => {
      it('should have a getDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.getDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getDeployment(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getDeployment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeployment - errors', () => {
      it('should have a deleteDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteDeployment(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteDeployment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigIssue - errors', () => {
      it('should have a getConfigIssue function', (done) => {
        try {
          assert.equal(true, typeof a.getConfigIssue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getConfigIssue(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getConfigIssue', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJoinHAStatusList - errors', () => {
      it('should have a getJoinHAStatusList function', (done) => {
        try {
          assert.equal(true, typeof a.getJoinHAStatusList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addJoinHAStatus - errors', () => {
      it('should have a addJoinHAStatus function', (done) => {
        try {
          assert.equal(true, typeof a.addJoinHAStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJoinHAStatus - errors', () => {
      it('should have a getJoinHAStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getJoinHAStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getJoinHAStatus(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getJoinHAStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBreakHAStatusList - errors', () => {
      it('should have a getBreakHAStatusList function', (done) => {
        try {
          assert.equal(true, typeof a.getBreakHAStatusList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addBreakHAStatus - errors', () => {
      it('should have a addBreakHAStatus function', (done) => {
        try {
          assert.equal(true, typeof a.addBreakHAStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBreakHAStatus - errors', () => {
      it('should have a getBreakHAStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getBreakHAStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getBreakHAStatus(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getBreakHAStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#startHAResume - errors', () => {
      it('should have a startHAResume function', (done) => {
        try {
          assert.equal(true, typeof a.startHAResume === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#startHASuspend - errors', () => {
      it('should have a startHASuspend function', (done) => {
        try {
          assert.equal(true, typeof a.startHASuspend === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#startHAFailover - errors', () => {
      it('should have a startHAFailover function', (done) => {
        try {
          assert.equal(true, typeof a.startHAFailover === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addHAStatus - errors', () => {
      it('should have a addHAStatus function', (done) => {
        try {
          assert.equal(true, typeof a.addHAStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeploymentData - errors', () => {
      it('should have a getDeploymentData function', (done) => {
        try {
          assert.equal(true, typeof a.getDeploymentData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getDeploymentData(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getDeploymentData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrafficInterruptionReasons - errors', () => {
      it('should have a getTrafficInterruptionReasons function', (done) => {
        try {
          assert.equal(true, typeof a.getTrafficInterruptionReasons === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getTrafficInterruptionReasons(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getTrafficInterruptionReasons', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClipboard - errors', () => {
      it('should have a getClipboard function', (done) => {
        try {
          assert.equal(true, typeof a.getClipboard === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getClipboard(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getClipboard', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getdownload - errors', () => {
      it('should have a getdownload function', (done) => {
        try {
          assert.equal(true, typeof a.getdownload === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getdownload(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getdownload', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBaseEntityDiffList - errors', () => {
      it('should have a getBaseEntityDiffList function', (done) => {
        try {
          assert.equal(true, typeof a.getBaseEntityDiffList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBaseEntityDiff - errors', () => {
      it('should have a deleteBaseEntityDiff function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBaseEntityDiff === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getdownloadconfig - errors', () => {
      it('should have a getdownloadconfig function', (done) => {
        try {
          assert.equal(true, typeof a.getdownloadconfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getdownloadconfig(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getdownloadconfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getdownloaddiskfile - errors', () => {
      it('should have a getdownloaddiskfile function', (done) => {
        try {
          assert.equal(true, typeof a.getdownloaddiskfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getdownloaddiskfile(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getdownloaddiskfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getdownloadinternalcacertificate - errors', () => {
      it('should have a getdownloadinternalcacertificate function', (done) => {
        try {
          assert.equal(true, typeof a.getdownloadinternalcacertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getdownloadinternalcacertificate(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getdownloadinternalcacertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getdownloadtroubleshoot - errors', () => {
      it('should have a getdownloadtroubleshoot function', (done) => {
        try {
          assert.equal(true, typeof a.getdownloadtroubleshoot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getdownloadtroubleshoot(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getdownloadtroubleshoot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getdownloadbackup - errors', () => {
      it('should have a getdownloadbackup function', (done) => {
        try {
          assert.equal(true, typeof a.getdownloadbackup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getdownloadbackup(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getdownloadbackup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScheduleExportConfigList - errors', () => {
      it('should have a getScheduleExportConfigList function', (done) => {
        try {
          assert.equal(true, typeof a.getScheduleExportConfigList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addScheduleExportConfig - errors', () => {
      it('should have a addScheduleExportConfig function', (done) => {
        try {
          assert.equal(true, typeof a.addScheduleExportConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addScheduleExportConfig(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addScheduleExportConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExportConfigJobHistoryList - errors', () => {
      it('should have a getExportConfigJobHistoryList function', (done) => {
        try {
          assert.equal(true, typeof a.getExportConfigJobHistoryList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExportConfigJobHistory - errors', () => {
      it('should have a getExportConfigJobHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getExportConfigJobHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getExportConfigJobHistory(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getExportConfigJobHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExportConfigJobHistory - errors', () => {
      it('should have a deleteExportConfigJobHistory function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExportConfigJobHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteExportConfigJobHistory(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteExportConfigJobHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuditEventList - errors', () => {
      it('should have a getAuditEventList function', (done) => {
        try {
          assert.equal(true, typeof a.getAuditEventList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuditEvent - errors', () => {
      it('should have a getAuditEvent function', (done) => {
        try {
          assert.equal(true, typeof a.getAuditEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getAuditEvent(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getAuditEvent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuditEntityChangeList - errors', () => {
      it('should have a getAuditEntityChangeList function', (done) => {
        try {
          assert.equal(true, typeof a.getAuditEntityChangeList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.getAuditEntityChangeList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getAuditEntityChangeList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCliDeploymentError - errors', () => {
      it('should have a getCliDeploymentError function', (done) => {
        try {
          assert.equal(true, typeof a.getCliDeploymentError === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getCliDeploymentError(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getCliDeploymentError', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBackupImmediateList - errors', () => {
      it('should have a getBackupImmediateList function', (done) => {
        try {
          assert.equal(true, typeof a.getBackupImmediateList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addBackupImmediate - errors', () => {
      it('should have a addBackupImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.addBackupImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addBackupImmediate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addBackupImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBackupImmediate - errors', () => {
      it('should have a getBackupImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.getBackupImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getBackupImmediate(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getBackupImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editBackupImmediate - errors', () => {
      it('should have a editBackupImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.editBackupImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editBackupImmediate(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editBackupImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editBackupImmediate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editBackupImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBackupImmediate - errors', () => {
      it('should have a deleteBackupImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBackupImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteBackupImmediate(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteBackupImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBackupScheduledList - errors', () => {
      it('should have a getBackupScheduledList function', (done) => {
        try {
          assert.equal(true, typeof a.getBackupScheduledList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addBackupScheduled - errors', () => {
      it('should have a addBackupScheduled function', (done) => {
        try {
          assert.equal(true, typeof a.addBackupScheduled === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addBackupScheduled(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addBackupScheduled', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBackupScheduled - errors', () => {
      it('should have a getBackupScheduled function', (done) => {
        try {
          assert.equal(true, typeof a.getBackupScheduled === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getBackupScheduled(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getBackupScheduled', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editBackupScheduled - errors', () => {
      it('should have a editBackupScheduled function', (done) => {
        try {
          assert.equal(true, typeof a.editBackupScheduled === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editBackupScheduled(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editBackupScheduled', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editBackupScheduled('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editBackupScheduled', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBackupScheduled - errors', () => {
      it('should have a deleteBackupScheduled function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBackupScheduled === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteBackupScheduled(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteBackupScheduled', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestoreImmediateList - errors', () => {
      it('should have a getRestoreImmediateList function', (done) => {
        try {
          assert.equal(true, typeof a.getRestoreImmediateList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addRestoreImmediate - errors', () => {
      it('should have a addRestoreImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.addRestoreImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addRestoreImmediate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addRestoreImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestoreImmediate - errors', () => {
      it('should have a getRestoreImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.getRestoreImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getRestoreImmediate(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getRestoreImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editRestoreImmediate - errors', () => {
      it('should have a editRestoreImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.editRestoreImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editRestoreImmediate(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editRestoreImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editRestoreImmediate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editRestoreImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestoreImmediate - errors', () => {
      it('should have a deleteRestoreImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestoreImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteRestoreImmediate(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteRestoreImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getArchivedBackupList - errors', () => {
      it('should have a getArchivedBackupList function', (done) => {
        try {
          assert.equal(true, typeof a.getArchivedBackupList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getArchivedBackup - errors', () => {
      it('should have a getArchivedBackup function', (done) => {
        try {
          assert.equal(true, typeof a.getArchivedBackup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getArchivedBackup(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getArchivedBackup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteArchivedBackup - errors', () => {
      it('should have a deleteArchivedBackup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteArchivedBackup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteArchivedBackup(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteArchivedBackup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGeolocationUpdateImmediateList - errors', () => {
      it('should have a getGeolocationUpdateImmediateList function', (done) => {
        try {
          assert.equal(true, typeof a.getGeolocationUpdateImmediateList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addGeolocationUpdateImmediate - errors', () => {
      it('should have a addGeolocationUpdateImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.addGeolocationUpdateImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addGeolocationUpdateImmediate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addGeolocationUpdateImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGeolocationUpdateImmediate - errors', () => {
      it('should have a getGeolocationUpdateImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.getGeolocationUpdateImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getGeolocationUpdateImmediate(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getGeolocationUpdateImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editGeolocationUpdateImmediate - errors', () => {
      it('should have a editGeolocationUpdateImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.editGeolocationUpdateImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editGeolocationUpdateImmediate(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editGeolocationUpdateImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editGeolocationUpdateImmediate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editGeolocationUpdateImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGeolocationUpdateImmediate - errors', () => {
      it('should have a deleteGeolocationUpdateImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteGeolocationUpdateImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteGeolocationUpdateImmediate(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteGeolocationUpdateImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGeolocationUpdateScheduleList - errors', () => {
      it('should have a getGeolocationUpdateScheduleList function', (done) => {
        try {
          assert.equal(true, typeof a.getGeolocationUpdateScheduleList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addGeolocationUpdateSchedule - errors', () => {
      it('should have a addGeolocationUpdateSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.addGeolocationUpdateSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addGeolocationUpdateSchedule(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addGeolocationUpdateSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGeolocationUpdateSchedule - errors', () => {
      it('should have a getGeolocationUpdateSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.getGeolocationUpdateSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getGeolocationUpdateSchedule(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getGeolocationUpdateSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editGeolocationUpdateSchedule - errors', () => {
      it('should have a editGeolocationUpdateSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.editGeolocationUpdateSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editGeolocationUpdateSchedule(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editGeolocationUpdateSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editGeolocationUpdateSchedule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editGeolocationUpdateSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGeolocationUpdateSchedule - errors', () => {
      it('should have a deleteGeolocationUpdateSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteGeolocationUpdateSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteGeolocationUpdateSchedule(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteGeolocationUpdateSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postupdategeolocationfromfile - errors', () => {
      it('should have a postupdategeolocationfromfile function', (done) => {
        try {
          assert.equal(true, typeof a.postupdategeolocationfromfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSRUUpdateImmediateList - errors', () => {
      it('should have a getSRUUpdateImmediateList function', (done) => {
        try {
          assert.equal(true, typeof a.getSRUUpdateImmediateList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSRUUpdateImmediate - errors', () => {
      it('should have a addSRUUpdateImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.addSRUUpdateImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSRUUpdateImmediate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addSRUUpdateImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSRUUpdateImmediate - errors', () => {
      it('should have a getSRUUpdateImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.getSRUUpdateImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getSRUUpdateImmediate(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSRUUpdateImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editSRUUpdateImmediate - errors', () => {
      it('should have a editSRUUpdateImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.editSRUUpdateImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editSRUUpdateImmediate(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSRUUpdateImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editSRUUpdateImmediate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSRUUpdateImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSRUUpdateImmediate - errors', () => {
      it('should have a deleteSRUUpdateImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSRUUpdateImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteSRUUpdateImmediate(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteSRUUpdateImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSRUUpdateScheduleList - errors', () => {
      it('should have a getSRUUpdateScheduleList function', (done) => {
        try {
          assert.equal(true, typeof a.getSRUUpdateScheduleList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSRUUpdateSchedule - errors', () => {
      it('should have a addSRUUpdateSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.addSRUUpdateSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSRUUpdateSchedule(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addSRUUpdateSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSRUUpdateSchedule - errors', () => {
      it('should have a getSRUUpdateSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.getSRUUpdateSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getSRUUpdateSchedule(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSRUUpdateSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editSRUUpdateSchedule - errors', () => {
      it('should have a editSRUUpdateSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.editSRUUpdateSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editSRUUpdateSchedule(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSRUUpdateSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editSRUUpdateSchedule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSRUUpdateSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSRUUpdateSchedule - errors', () => {
      it('should have a deleteSRUUpdateSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSRUUpdateSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteSRUUpdateSchedule(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteSRUUpdateSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSRUFileUpload - errors', () => {
      it('should have a addSRUFileUpload function', (done) => {
        try {
          assert.equal(true, typeof a.addSRUFileUpload === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVDBUpdateImmediateList - errors', () => {
      it('should have a getVDBUpdateImmediateList function', (done) => {
        try {
          assert.equal(true, typeof a.getVDBUpdateImmediateList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addVDBUpdateImmediate - errors', () => {
      it('should have a addVDBUpdateImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.addVDBUpdateImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addVDBUpdateImmediate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addVDBUpdateImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVDBUpdateImmediate - errors', () => {
      it('should have a getVDBUpdateImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.getVDBUpdateImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getVDBUpdateImmediate(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getVDBUpdateImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editVDBUpdateImmediate - errors', () => {
      it('should have a editVDBUpdateImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.editVDBUpdateImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editVDBUpdateImmediate(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editVDBUpdateImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editVDBUpdateImmediate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editVDBUpdateImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVDBUpdateImmediate - errors', () => {
      it('should have a deleteVDBUpdateImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVDBUpdateImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteVDBUpdateImmediate(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteVDBUpdateImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVDBUpdateScheduleList - errors', () => {
      it('should have a getVDBUpdateScheduleList function', (done) => {
        try {
          assert.equal(true, typeof a.getVDBUpdateScheduleList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addVDBUpdateSchedule - errors', () => {
      it('should have a addVDBUpdateSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.addVDBUpdateSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addVDBUpdateSchedule(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addVDBUpdateSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVDBUpdateSchedule - errors', () => {
      it('should have a getVDBUpdateSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.getVDBUpdateSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getVDBUpdateSchedule(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getVDBUpdateSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editVDBUpdateSchedule - errors', () => {
      it('should have a editVDBUpdateSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.editVDBUpdateSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editVDBUpdateSchedule(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editVDBUpdateSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editVDBUpdateSchedule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editVDBUpdateSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVDBUpdateSchedule - errors', () => {
      it('should have a deleteVDBUpdateSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVDBUpdateSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteVDBUpdateSchedule(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteVDBUpdateSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postupdatevdbfromfile - errors', () => {
      it('should have a postupdatevdbfromfile function', (done) => {
        try {
          assert.equal(true, typeof a.postupdatevdbfromfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPullUpgradeJobList - errors', () => {
      it('should have a getPullUpgradeJobList function', (done) => {
        try {
          assert.equal(true, typeof a.getPullUpgradeJobList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPullUpgradeJob - errors', () => {
      it('should have a getPullUpgradeJob function', (done) => {
        try {
          assert.equal(true, typeof a.getPullUpgradeJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getPullUpgradeJob(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getPullUpgradeJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePullUpgradeJob - errors', () => {
      it('should have a deletePullUpgradeJob function', (done) => {
        try {
          assert.equal(true, typeof a.deletePullUpgradeJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deletePullUpgradeJob(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deletePullUpgradeJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPullFileJobList - errors', () => {
      it('should have a getPullFileJobList function', (done) => {
        try {
          assert.equal(true, typeof a.getPullFileJobList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPullFileJob - errors', () => {
      it('should have a getPullFileJob function', (done) => {
        try {
          assert.equal(true, typeof a.getPullFileJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getPullFileJob(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getPullFileJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePullFileJob - errors', () => {
      it('should have a deletePullFileJob function', (done) => {
        try {
          assert.equal(true, typeof a.deletePullFileJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deletePullFileJob(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deletePullFileJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryBackupList - errors', () => {
      it('should have a getJobHistoryBackupList function', (done) => {
        try {
          assert.equal(true, typeof a.getJobHistoryBackupList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryBackup - errors', () => {
      it('should have a getJobHistoryBackup function', (done) => {
        try {
          assert.equal(true, typeof a.getJobHistoryBackup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getJobHistoryBackup(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getJobHistoryBackup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJobHistoryBackup - errors', () => {
      it('should have a deleteJobHistoryBackup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteJobHistoryBackup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteJobHistoryBackup(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteJobHistoryBackup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryGeolocationList - errors', () => {
      it('should have a getJobHistoryGeolocationList function', (done) => {
        try {
          assert.equal(true, typeof a.getJobHistoryGeolocationList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryGeolocation - errors', () => {
      it('should have a getJobHistoryGeolocation function', (done) => {
        try {
          assert.equal(true, typeof a.getJobHistoryGeolocation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getJobHistoryGeolocation(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getJobHistoryGeolocation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJobHistoryGeolocation - errors', () => {
      it('should have a deleteJobHistoryGeolocation function', (done) => {
        try {
          assert.equal(true, typeof a.deleteJobHistoryGeolocation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteJobHistoryGeolocation(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteJobHistoryGeolocation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistorySruUpdateList - errors', () => {
      it('should have a getJobHistorySruUpdateList function', (done) => {
        try {
          assert.equal(true, typeof a.getJobHistorySruUpdateList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistorySruUpdate - errors', () => {
      it('should have a getJobHistorySruUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.getJobHistorySruUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getJobHistorySruUpdate(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getJobHistorySruUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJobHistorySruUpdate - errors', () => {
      it('should have a deleteJobHistorySruUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteJobHistorySruUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteJobHistorySruUpdate(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteJobHistorySruUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryVDBUpdateList - errors', () => {
      it('should have a getJobHistoryVDBUpdateList function', (done) => {
        try {
          assert.equal(true, typeof a.getJobHistoryVDBUpdateList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryVDBUpdate - errors', () => {
      it('should have a getJobHistoryVDBUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.getJobHistoryVDBUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getJobHistoryVDBUpdate(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getJobHistoryVDBUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJobHistoryVDBUpdate - errors', () => {
      it('should have a deleteJobHistoryVDBUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteJobHistoryVDBUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteJobHistoryVDBUpdate(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteJobHistoryVDBUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryDeploymentList - errors', () => {
      it('should have a getJobHistoryDeploymentList function', (done) => {
        try {
          assert.equal(true, typeof a.getJobHistoryDeploymentList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryDeployment - errors', () => {
      it('should have a getJobHistoryDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.getJobHistoryDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getJobHistoryDeployment(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getJobHistoryDeployment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJobHistoryDeployment - errors', () => {
      it('should have a deleteJobHistoryDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.deleteJobHistoryDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteJobHistoryDeployment(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteJobHistoryDeployment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryHaConfigSyncList - errors', () => {
      it('should have a getJobHistoryHaConfigSyncList function', (done) => {
        try {
          assert.equal(true, typeof a.getJobHistoryHaConfigSyncList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryHaConfigSync - errors', () => {
      it('should have a getJobHistoryHaConfigSync function', (done) => {
        try {
          assert.equal(true, typeof a.getJobHistoryHaConfigSync === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getJobHistoryHaConfigSync(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getJobHistoryHaConfigSync', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJobHistoryHaConfigSync - errors', () => {
      it('should have a deleteJobHistoryHaConfigSync function', (done) => {
        try {
          assert.equal(true, typeof a.deleteJobHistoryHaConfigSync === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteJobHistoryHaConfigSync(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteJobHistoryHaConfigSync', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryCloudManagementList - errors', () => {
      it('should have a getJobHistoryCloudManagementList function', (done) => {
        try {
          assert.equal(true, typeof a.getJobHistoryCloudManagementList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryCloudManagement - errors', () => {
      it('should have a getJobHistoryCloudManagement function', (done) => {
        try {
          assert.equal(true, typeof a.getJobHistoryCloudManagement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getJobHistoryCloudManagement(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getJobHistoryCloudManagement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJobHistoryCloudManagement - errors', () => {
      it('should have a deleteJobHistoryCloudManagement function', (done) => {
        try {
          assert.equal(true, typeof a.deleteJobHistoryCloudManagement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteJobHistoryCloudManagement(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteJobHistoryCloudManagement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicenseJobHistoryList - errors', () => {
      it('should have a getLicenseJobHistoryList function', (done) => {
        try {
          assert.equal(true, typeof a.getLicenseJobHistoryList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicenseJobHistory - errors', () => {
      it('should have a getLicenseJobHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getLicenseJobHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getLicenseJobHistory(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getLicenseJobHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLicenseJobHistory - errors', () => {
      it('should have a deleteLicenseJobHistory function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLicenseJobHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteLicenseJobHistory(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteLicenseJobHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryEntityList - errors', () => {
      it('should have a getJobHistoryEntityList function', (done) => {
        try {
          assert.equal(true, typeof a.getJobHistoryEntityList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJobHistoryEntities - errors', () => {
      it('should have a deleteJobHistoryEntities function', (done) => {
        try {
          assert.equal(true, typeof a.deleteJobHistoryEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobHistoryEntity - errors', () => {
      it('should have a getJobHistoryEntity function', (done) => {
        try {
          assert.equal(true, typeof a.getJobHistoryEntity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getJobHistoryEntity(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getJobHistoryEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJobHistoryEntity - errors', () => {
      it('should have a deleteJobHistoryEntity function', (done) => {
        try {
          assert.equal(true, typeof a.deleteJobHistoryEntity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteJobHistoryEntity(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteJobHistoryEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRaVpnGroupPolicyList - errors', () => {
      it('should have a getRaVpnGroupPolicyList function', (done) => {
        try {
          assert.equal(true, typeof a.getRaVpnGroupPolicyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addRaVpnGroupPolicy - errors', () => {
      it('should have a addRaVpnGroupPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.addRaVpnGroupPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addRaVpnGroupPolicy(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addRaVpnGroupPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRaVpnGroupPolicy - errors', () => {
      it('should have a getRaVpnGroupPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getRaVpnGroupPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getRaVpnGroupPolicy(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getRaVpnGroupPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editRaVpnGroupPolicy - errors', () => {
      it('should have a editRaVpnGroupPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.editRaVpnGroupPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editRaVpnGroupPolicy(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editRaVpnGroupPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editRaVpnGroupPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editRaVpnGroupPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRaVpnGroupPolicy - errors', () => {
      it('should have a deleteRaVpnGroupPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRaVpnGroupPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteRaVpnGroupPolicy(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteRaVpnGroupPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLdapAttributeMapList - errors', () => {
      it('should have a getLdapAttributeMapList function', (done) => {
        try {
          assert.equal(true, typeof a.getLdapAttributeMapList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addLdapAttributeMap - errors', () => {
      it('should have a addLdapAttributeMap function', (done) => {
        try {
          assert.equal(true, typeof a.addLdapAttributeMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addLdapAttributeMap(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addLdapAttributeMap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLdapAttributeMap - errors', () => {
      it('should have a getLdapAttributeMap function', (done) => {
        try {
          assert.equal(true, typeof a.getLdapAttributeMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getLdapAttributeMap(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getLdapAttributeMap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editLdapAttributeMap - errors', () => {
      it('should have a editLdapAttributeMap function', (done) => {
        try {
          assert.equal(true, typeof a.editLdapAttributeMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editLdapAttributeMap(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editLdapAttributeMap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editLdapAttributeMap('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editLdapAttributeMap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLdapAttributeMap - errors', () => {
      it('should have a deleteLdapAttributeMap function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLdapAttributeMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteLdapAttributeMap(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteLdapAttributeMap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAnyConnectPackageFileList - errors', () => {
      it('should have a getAnyConnectPackageFileList function', (done) => {
        try {
          assert.equal(true, typeof a.getAnyConnectPackageFileList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addAnyConnectPackageFile - errors', () => {
      it('should have a addAnyConnectPackageFile function', (done) => {
        try {
          assert.equal(true, typeof a.addAnyConnectPackageFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addAnyConnectPackageFile(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addAnyConnectPackageFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAnyConnectPackageFile - errors', () => {
      it('should have a getAnyConnectPackageFile function', (done) => {
        try {
          assert.equal(true, typeof a.getAnyConnectPackageFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getAnyConnectPackageFile(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getAnyConnectPackageFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editAnyConnectPackageFile - errors', () => {
      it('should have a editAnyConnectPackageFile function', (done) => {
        try {
          assert.equal(true, typeof a.editAnyConnectPackageFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editAnyConnectPackageFile(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editAnyConnectPackageFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editAnyConnectPackageFile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editAnyConnectPackageFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAnyConnectPackageFile - errors', () => {
      it('should have a deleteAnyConnectPackageFile function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAnyConnectPackageFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteAnyConnectPackageFile(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteAnyConnectPackageFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRaVpnConnectionProfileList - errors', () => {
      it('should have a getRaVpnConnectionProfileList function', (done) => {
        try {
          assert.equal(true, typeof a.getRaVpnConnectionProfileList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.getRaVpnConnectionProfileList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getRaVpnConnectionProfileList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addRaVpnConnectionProfile - errors', () => {
      it('should have a addRaVpnConnectionProfile function', (done) => {
        try {
          assert.equal(true, typeof a.addRaVpnConnectionProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.addRaVpnConnectionProfile(null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addRaVpnConnectionProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addRaVpnConnectionProfile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addRaVpnConnectionProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRaVpnConnectionProfile - errors', () => {
      it('should have a getRaVpnConnectionProfile function', (done) => {
        try {
          assert.equal(true, typeof a.getRaVpnConnectionProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.getRaVpnConnectionProfile(null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getRaVpnConnectionProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getRaVpnConnectionProfile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getRaVpnConnectionProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editRaVpnConnectionProfile - errors', () => {
      it('should have a editRaVpnConnectionProfile function', (done) => {
        try {
          assert.equal(true, typeof a.editRaVpnConnectionProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.editRaVpnConnectionProfile(null, null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editRaVpnConnectionProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editRaVpnConnectionProfile('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editRaVpnConnectionProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editRaVpnConnectionProfile('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editRaVpnConnectionProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRaVpnConnectionProfile - errors', () => {
      it('should have a deleteRaVpnConnectionProfile function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRaVpnConnectionProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.deleteRaVpnConnectionProfile(null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteRaVpnConnectionProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteRaVpnConnectionProfile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteRaVpnConnectionProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRaVpnList - errors', () => {
      it('should have a getRaVpnList function', (done) => {
        try {
          assert.equal(true, typeof a.getRaVpnList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addRaVpn - errors', () => {
      it('should have a addRaVpn function', (done) => {
        try {
          assert.equal(true, typeof a.addRaVpn === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addRaVpn(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addRaVpn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRaVpn - errors', () => {
      it('should have a getRaVpn function', (done) => {
        try {
          assert.equal(true, typeof a.getRaVpn === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getRaVpn(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getRaVpn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editRaVpn - errors', () => {
      it('should have a editRaVpn function', (done) => {
        try {
          assert.equal(true, typeof a.editRaVpn === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editRaVpn(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editRaVpn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editRaVpn('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editRaVpn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRaVpn - errors', () => {
      it('should have a deleteRaVpn function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRaVpn === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteRaVpn(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteRaVpn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccessRuleList - errors', () => {
      it('should have a getAccessRuleList function', (done) => {
        try {
          assert.equal(true, typeof a.getAccessRuleList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.getAccessRuleList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getAccessRuleList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addAccessRule - errors', () => {
      it('should have a addAccessRule function', (done) => {
        try {
          assert.equal(true, typeof a.addAccessRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.addAccessRule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addAccessRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addAccessRule('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addAccessRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccessRule - errors', () => {
      it('should have a getAccessRule function', (done) => {
        try {
          assert.equal(true, typeof a.getAccessRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.getAccessRule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getAccessRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getAccessRule('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getAccessRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editAccessRule - errors', () => {
      it('should have a editAccessRule function', (done) => {
        try {
          assert.equal(true, typeof a.editAccessRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.editAccessRule('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editAccessRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editAccessRule('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editAccessRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editAccessRule('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editAccessRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAccessRule - errors', () => {
      it('should have a deleteAccessRule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAccessRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.deleteAccessRule(null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteAccessRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteAccessRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteAccessRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccessPolicyList - errors', () => {
      it('should have a getAccessPolicyList function', (done) => {
        try {
          assert.equal(true, typeof a.getAccessPolicyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccessPolicy - errors', () => {
      it('should have a getAccessPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getAccessPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getAccessPolicy(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getAccessPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editAccessPolicy - errors', () => {
      it('should have a editAccessPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.editAccessPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editAccessPolicy(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editAccessPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editAccessPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editAccessPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getManualNatRuleList - errors', () => {
      it('should have a getManualNatRuleList function', (done) => {
        try {
          assert.equal(true, typeof a.getManualNatRuleList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.getManualNatRuleList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getManualNatRuleList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addManualNatRule - errors', () => {
      it('should have a addManualNatRule function', (done) => {
        try {
          assert.equal(true, typeof a.addManualNatRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.addManualNatRule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addManualNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addManualNatRule('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addManualNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getManualNatRule - errors', () => {
      it('should have a getManualNatRule function', (done) => {
        try {
          assert.equal(true, typeof a.getManualNatRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.getManualNatRule(null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getManualNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getManualNatRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getManualNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editManualNatRule - errors', () => {
      it('should have a editManualNatRule function', (done) => {
        try {
          assert.equal(true, typeof a.editManualNatRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.editManualNatRule('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editManualNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editManualNatRule('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editManualNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editManualNatRule('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editManualNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteManualNatRule - errors', () => {
      it('should have a deleteManualNatRule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteManualNatRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.deleteManualNatRule(null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteManualNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteManualNatRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteManualNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getManualNatRuleContainerList - errors', () => {
      it('should have a getManualNatRuleContainerList function', (done) => {
        try {
          assert.equal(true, typeof a.getManualNatRuleContainerList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getManualNatRuleContainer - errors', () => {
      it('should have a getManualNatRuleContainer function', (done) => {
        try {
          assert.equal(true, typeof a.getManualNatRuleContainer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getManualNatRuleContainer(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getManualNatRuleContainer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectNatRuleList - errors', () => {
      it('should have a getObjectNatRuleList function', (done) => {
        try {
          assert.equal(true, typeof a.getObjectNatRuleList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.getObjectNatRuleList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getObjectNatRuleList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addObjectNatRule - errors', () => {
      it('should have a addObjectNatRule function', (done) => {
        try {
          assert.equal(true, typeof a.addObjectNatRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.addObjectNatRule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addObjectNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addObjectNatRule('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addObjectNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectNatRule - errors', () => {
      it('should have a getObjectNatRule function', (done) => {
        try {
          assert.equal(true, typeof a.getObjectNatRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.getObjectNatRule(null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getObjectNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getObjectNatRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getObjectNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editObjectNatRule - errors', () => {
      it('should have a editObjectNatRule function', (done) => {
        try {
          assert.equal(true, typeof a.editObjectNatRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.editObjectNatRule('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editObjectNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editObjectNatRule('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editObjectNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editObjectNatRule('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editObjectNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteObjectNatRule - errors', () => {
      it('should have a deleteObjectNatRule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteObjectNatRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.deleteObjectNatRule(null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteObjectNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteObjectNatRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteObjectNatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectNatRuleContainerList - errors', () => {
      it('should have a getObjectNatRuleContainerList function', (done) => {
        try {
          assert.equal(true, typeof a.getObjectNatRuleContainerList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectNatRuleContainer - errors', () => {
      it('should have a getObjectNatRuleContainer function', (done) => {
        try {
          assert.equal(true, typeof a.getObjectNatRuleContainer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getObjectNatRuleContainer(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getObjectNatRuleContainer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postuploaddiskfile - errors', () => {
      it('should have a postuploaddiskfile function', (done) => {
        try {
          assert.equal(true, typeof a.postuploaddiskfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postuploadbackup - errors', () => {
      it('should have a postuploadbackup function', (done) => {
        try {
          assert.equal(true, typeof a.postuploadbackup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postuploadupgrade - errors', () => {
      it('should have a postuploadupgrade function', (done) => {
        try {
          assert.equal(true, typeof a.postuploadupgrade === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAnyConnectClientProfileList - errors', () => {
      it('should have a getAnyConnectClientProfileList function', (done) => {
        try {
          assert.equal(true, typeof a.getAnyConnectClientProfileList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addAnyConnectClientProfile - errors', () => {
      it('should have a addAnyConnectClientProfile function', (done) => {
        try {
          assert.equal(true, typeof a.addAnyConnectClientProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addAnyConnectClientProfile(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addAnyConnectClientProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAnyConnectClientProfile - errors', () => {
      it('should have a getAnyConnectClientProfile function', (done) => {
        try {
          assert.equal(true, typeof a.getAnyConnectClientProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getAnyConnectClientProfile(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getAnyConnectClientProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editAnyConnectClientProfile - errors', () => {
      it('should have a editAnyConnectClientProfile function', (done) => {
        try {
          assert.equal(true, typeof a.editAnyConnectClientProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editAnyConnectClientProfile(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editAnyConnectClientProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editAnyConnectClientProfile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editAnyConnectClientProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAnyConnectClientProfile - errors', () => {
      it('should have a deleteAnyConnectClientProfile function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAnyConnectClientProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteAnyConnectClientProfile(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteAnyConnectClientProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIkevOnePolicyList - errors', () => {
      it('should have a getIkevOnePolicyList function', (done) => {
        try {
          assert.equal(true, typeof a.getIkevOnePolicyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addIkevOnePolicy - errors', () => {
      it('should have a addIkevOnePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.addIkevOnePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addIkevOnePolicy(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addIkevOnePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIkevOnePolicy - errors', () => {
      it('should have a getIkevOnePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getIkevOnePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getIkevOnePolicy(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getIkevOnePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editIkevOnePolicy - errors', () => {
      it('should have a editIkevOnePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.editIkevOnePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editIkevOnePolicy(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editIkevOnePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editIkevOnePolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editIkevOnePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIkevOnePolicy - errors', () => {
      it('should have a deleteIkevOnePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIkevOnePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteIkevOnePolicy(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteIkevOnePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIkevOneProposalList - errors', () => {
      it('should have a getIkevOneProposalList function', (done) => {
        try {
          assert.equal(true, typeof a.getIkevOneProposalList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addIkevOneProposal - errors', () => {
      it('should have a addIkevOneProposal function', (done) => {
        try {
          assert.equal(true, typeof a.addIkevOneProposal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addIkevOneProposal(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addIkevOneProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIkevOneProposal - errors', () => {
      it('should have a getIkevOneProposal function', (done) => {
        try {
          assert.equal(true, typeof a.getIkevOneProposal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getIkevOneProposal(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getIkevOneProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editIkevOneProposal - errors', () => {
      it('should have a editIkevOneProposal function', (done) => {
        try {
          assert.equal(true, typeof a.editIkevOneProposal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editIkevOneProposal(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editIkevOneProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editIkevOneProposal('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editIkevOneProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIkevOneProposal - errors', () => {
      it('should have a deleteIkevOneProposal function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIkevOneProposal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteIkevOneProposal(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteIkevOneProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIkevTwoPolicyList - errors', () => {
      it('should have a getIkevTwoPolicyList function', (done) => {
        try {
          assert.equal(true, typeof a.getIkevTwoPolicyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addIkevTwoPolicy - errors', () => {
      it('should have a addIkevTwoPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.addIkevTwoPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addIkevTwoPolicy(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addIkevTwoPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIkevTwoPolicy - errors', () => {
      it('should have a getIkevTwoPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getIkevTwoPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getIkevTwoPolicy(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getIkevTwoPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editIkevTwoPolicy - errors', () => {
      it('should have a editIkevTwoPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.editIkevTwoPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editIkevTwoPolicy(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editIkevTwoPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editIkevTwoPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editIkevTwoPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIkevTwoPolicy - errors', () => {
      it('should have a deleteIkevTwoPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIkevTwoPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteIkevTwoPolicy(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteIkevTwoPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIkevTwoProposalList - errors', () => {
      it('should have a getIkevTwoProposalList function', (done) => {
        try {
          assert.equal(true, typeof a.getIkevTwoProposalList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addIkevTwoProposal - errors', () => {
      it('should have a addIkevTwoProposal function', (done) => {
        try {
          assert.equal(true, typeof a.addIkevTwoProposal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addIkevTwoProposal(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addIkevTwoProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIkevTwoProposal - errors', () => {
      it('should have a getIkevTwoProposal function', (done) => {
        try {
          assert.equal(true, typeof a.getIkevTwoProposal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getIkevTwoProposal(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getIkevTwoProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editIkevTwoProposal - errors', () => {
      it('should have a editIkevTwoProposal function', (done) => {
        try {
          assert.equal(true, typeof a.editIkevTwoProposal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editIkevTwoProposal(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editIkevTwoProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editIkevTwoProposal('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editIkevTwoProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIkevTwoProposal - errors', () => {
      it('should have a deleteIkevTwoProposal function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIkevTwoProposal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteIkevTwoProposal(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteIkevTwoProposal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSToSConnectionProfileList - errors', () => {
      it('should have a getSToSConnectionProfileList function', (done) => {
        try {
          assert.equal(true, typeof a.getSToSConnectionProfileList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSToSConnectionProfile - errors', () => {
      it('should have a addSToSConnectionProfile function', (done) => {
        try {
          assert.equal(true, typeof a.addSToSConnectionProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSToSConnectionProfile(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addSToSConnectionProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSToSConnectionProfile - errors', () => {
      it('should have a getSToSConnectionProfile function', (done) => {
        try {
          assert.equal(true, typeof a.getSToSConnectionProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getSToSConnectionProfile(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSToSConnectionProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editSToSConnectionProfile - errors', () => {
      it('should have a editSToSConnectionProfile function', (done) => {
        try {
          assert.equal(true, typeof a.editSToSConnectionProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editSToSConnectionProfile(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSToSConnectionProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editSToSConnectionProfile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSToSConnectionProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSToSConnectionProfile - errors', () => {
      it('should have a deleteSToSConnectionProfile function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSToSConnectionProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteSToSConnectionProfile(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteSToSConnectionProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserList - errors', () => {
      it('should have a getUserList function', (done) => {
        try {
          assert.equal(true, typeof a.getUserList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addUser - errors', () => {
      it('should have a addUser function', (done) => {
        try {
          assert.equal(true, typeof a.addUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addUser(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUser - errors', () => {
      it('should have a getUser function', (done) => {
        try {
          assert.equal(true, typeof a.getUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getUser(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editUser - errors', () => {
      it('should have a editUser function', (done) => {
        try {
          assert.equal(true, typeof a.editUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editUser(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editUser('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUser - errors', () => {
      it('should have a deleteUser function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteUser(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNTPList - errors', () => {
      it('should have a getNTPList function', (done) => {
        try {
          assert.equal(true, typeof a.getNTPList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNTP - errors', () => {
      it('should have a getNTP function', (done) => {
        try {
          assert.equal(true, typeof a.getNTP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getNTP(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getNTP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editNTP - errors', () => {
      it('should have a editNTP function', (done) => {
        try {
          assert.equal(true, typeof a.editNTP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editNTP(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editNTP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editNTP('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editNTP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceLogSettingsList - errors', () => {
      it('should have a getDeviceLogSettingsList function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceLogSettingsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceLogSettings - errors', () => {
      it('should have a getDeviceLogSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceLogSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getDeviceLogSettings(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getDeviceLogSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editDeviceLogSettings - errors', () => {
      it('should have a editDeviceLogSettings function', (done) => {
        try {
          assert.equal(true, typeof a.editDeviceLogSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editDeviceLogSettings(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editDeviceLogSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editDeviceLogSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editDeviceLogSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSyslogServerList - errors', () => {
      it('should have a getSyslogServerList function', (done) => {
        try {
          assert.equal(true, typeof a.getSyslogServerList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSyslogServer - errors', () => {
      it('should have a addSyslogServer function', (done) => {
        try {
          assert.equal(true, typeof a.addSyslogServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSyslogServer(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addSyslogServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSyslogServer - errors', () => {
      it('should have a getSyslogServer function', (done) => {
        try {
          assert.equal(true, typeof a.getSyslogServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getSyslogServer(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSyslogServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editSyslogServer - errors', () => {
      it('should have a editSyslogServer function', (done) => {
        try {
          assert.equal(true, typeof a.editSyslogServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editSyslogServer(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSyslogServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editSyslogServer('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSyslogServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSyslogServer - errors', () => {
      it('should have a deleteSyslogServer function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSyslogServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteSyslogServer(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteSyslogServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceDNSSettingsList - errors', () => {
      it('should have a getDeviceDNSSettingsList function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceDNSSettingsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceDNSSettings - errors', () => {
      it('should have a getDeviceDNSSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceDNSSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getDeviceDNSSettings(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getDeviceDNSSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editDeviceDNSSettings - errors', () => {
      it('should have a editDeviceDNSSettings function', (done) => {
        try {
          assert.equal(true, typeof a.editDeviceDNSSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editDeviceDNSSettings(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editDeviceDNSSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editDeviceDNSSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editDeviceDNSSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDataDNSSettingsList - errors', () => {
      it('should have a getDataDNSSettingsList function', (done) => {
        try {
          assert.equal(true, typeof a.getDataDNSSettingsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDataDNSSettings - errors', () => {
      it('should have a getDataDNSSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getDataDNSSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getDataDNSSettings(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getDataDNSSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editDataDNSSettings - errors', () => {
      it('should have a editDataDNSSettings function', (done) => {
        try {
          assert.equal(true, typeof a.editDataDNSSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editDataDNSSettings(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editDataDNSSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editDataDNSSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editDataDNSSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDNSServerGroupList - errors', () => {
      it('should have a getDNSServerGroupList function', (done) => {
        try {
          assert.equal(true, typeof a.getDNSServerGroupList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addDNSServerGroup - errors', () => {
      it('should have a addDNSServerGroup function', (done) => {
        try {
          assert.equal(true, typeof a.addDNSServerGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addDNSServerGroup(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addDNSServerGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDNSServerGroup - errors', () => {
      it('should have a getDNSServerGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getDNSServerGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getDNSServerGroup(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getDNSServerGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editDNSServerGroup - errors', () => {
      it('should have a editDNSServerGroup function', (done) => {
        try {
          assert.equal(true, typeof a.editDNSServerGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editDNSServerGroup(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editDNSServerGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editDNSServerGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editDNSServerGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDNSServerGroup - errors', () => {
      it('should have a deleteDNSServerGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDNSServerGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteDNSServerGroup(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteDNSServerGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDataSSLCipherSettingList - errors', () => {
      it('should have a getDataSSLCipherSettingList function', (done) => {
        try {
          assert.equal(true, typeof a.getDataSSLCipherSettingList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDataSSLCipherSetting - errors', () => {
      it('should have a getDataSSLCipherSetting function', (done) => {
        try {
          assert.equal(true, typeof a.getDataSSLCipherSetting === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getDataSSLCipherSetting(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getDataSSLCipherSetting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editDataSSLCipherSetting - errors', () => {
      it('should have a editDataSSLCipherSetting function', (done) => {
        try {
          assert.equal(true, typeof a.editDataSSLCipherSetting === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editDataSSLCipherSetting(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editDataSSLCipherSetting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editDataSSLCipherSetting('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editDataSSLCipherSetting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSSLCipherList - errors', () => {
      it('should have a getSSLCipherList function', (done) => {
        try {
          assert.equal(true, typeof a.getSSLCipherList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSSLCipher - errors', () => {
      it('should have a addSSLCipher function', (done) => {
        try {
          assert.equal(true, typeof a.addSSLCipher === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSSLCipher(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addSSLCipher', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSSLCipher - errors', () => {
      it('should have a getSSLCipher function', (done) => {
        try {
          assert.equal(true, typeof a.getSSLCipher === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getSSLCipher(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSSLCipher', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editSSLCipher - errors', () => {
      it('should have a editSSLCipher function', (done) => {
        try {
          assert.equal(true, typeof a.editSSLCipher === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editSSLCipher(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSSLCipher', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editSSLCipher('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSSLCipher', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSSLCipher - errors', () => {
      it('should have a deleteSSLCipher function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSSLCipher === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteSSLCipher(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteSSLCipher', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOpenSSLCipherInfoList - errors', () => {
      it('should have a getOpenSSLCipherInfoList function', (done) => {
        try {
          assert.equal(true, typeof a.getOpenSSLCipherInfoList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityRuleList - errors', () => {
      it('should have a getIdentityRuleList function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityRuleList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.getIdentityRuleList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getIdentityRuleList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addIdentityRule - errors', () => {
      it('should have a addIdentityRule function', (done) => {
        try {
          assert.equal(true, typeof a.addIdentityRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.addIdentityRule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addIdentityRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addIdentityRule('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addIdentityRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityRule - errors', () => {
      it('should have a getIdentityRule function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.getIdentityRule(null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getIdentityRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getIdentityRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getIdentityRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editIdentityRule - errors', () => {
      it('should have a editIdentityRule function', (done) => {
        try {
          assert.equal(true, typeof a.editIdentityRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.editIdentityRule('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editIdentityRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editIdentityRule('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editIdentityRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editIdentityRule('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editIdentityRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIdentityRule - errors', () => {
      it('should have a deleteIdentityRule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIdentityRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.deleteIdentityRule(null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteIdentityRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteIdentityRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteIdentityRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityPolicyList - errors', () => {
      it('should have a getIdentityPolicyList function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityPolicyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityPolicy - errors', () => {
      it('should have a getIdentityPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getIdentityPolicy(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getIdentityPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editIdentityPolicy - errors', () => {
      it('should have a editIdentityPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.editIdentityPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editIdentityPolicy(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editIdentityPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editIdentityPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editIdentityPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInternalCertificateList - errors', () => {
      it('should have a getInternalCertificateList function', (done) => {
        try {
          assert.equal(true, typeof a.getInternalCertificateList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addInternalCertificate - errors', () => {
      it('should have a addInternalCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.addInternalCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addInternalCertificate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addInternalCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInternalCertificate - errors', () => {
      it('should have a getInternalCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.getInternalCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getInternalCertificate(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getInternalCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editInternalCertificate - errors', () => {
      it('should have a editInternalCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.editInternalCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editInternalCertificate(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editInternalCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editInternalCertificate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editInternalCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInternalCertificate - errors', () => {
      it('should have a deleteInternalCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteInternalCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteInternalCertificate(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteInternalCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInternalCACertificateList - errors', () => {
      it('should have a getInternalCACertificateList function', (done) => {
        try {
          assert.equal(true, typeof a.getInternalCACertificateList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addInternalCACertificate - errors', () => {
      it('should have a addInternalCACertificate function', (done) => {
        try {
          assert.equal(true, typeof a.addInternalCACertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addInternalCACertificate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addInternalCACertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInternalCACertificate - errors', () => {
      it('should have a getInternalCACertificate function', (done) => {
        try {
          assert.equal(true, typeof a.getInternalCACertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getInternalCACertificate(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getInternalCACertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editInternalCACertificate - errors', () => {
      it('should have a editInternalCACertificate function', (done) => {
        try {
          assert.equal(true, typeof a.editInternalCACertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editInternalCACertificate(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editInternalCACertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editInternalCACertificate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editInternalCACertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInternalCACertificate - errors', () => {
      it('should have a deleteInternalCACertificate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteInternalCACertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteInternalCACertificate(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteInternalCACertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExternalCertificateList - errors', () => {
      it('should have a getExternalCertificateList function', (done) => {
        try {
          assert.equal(true, typeof a.getExternalCertificateList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addExternalCertificate - errors', () => {
      it('should have a addExternalCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.addExternalCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addExternalCertificate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addExternalCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExternalCertificate - errors', () => {
      it('should have a getExternalCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.getExternalCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getExternalCertificate(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getExternalCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editExternalCertificate - errors', () => {
      it('should have a editExternalCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.editExternalCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editExternalCertificate(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editExternalCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editExternalCertificate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editExternalCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExternalCertificate - errors', () => {
      it('should have a deleteExternalCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExternalCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteExternalCertificate(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteExternalCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExternalCACertificateList - errors', () => {
      it('should have a getExternalCACertificateList function', (done) => {
        try {
          assert.equal(true, typeof a.getExternalCACertificateList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addExternalCACertificate - errors', () => {
      it('should have a addExternalCACertificate function', (done) => {
        try {
          assert.equal(true, typeof a.addExternalCACertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addExternalCACertificate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addExternalCACertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExternalCACertificate - errors', () => {
      it('should have a getExternalCACertificate function', (done) => {
        try {
          assert.equal(true, typeof a.getExternalCACertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getExternalCACertificate(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getExternalCACertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editExternalCACertificate - errors', () => {
      it('should have a editExternalCACertificate function', (done) => {
        try {
          assert.equal(true, typeof a.editExternalCACertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editExternalCACertificate(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editExternalCACertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editExternalCACertificate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editExternalCACertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExternalCACertificate - errors', () => {
      it('should have a deleteExternalCACertificate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExternalCACertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteExternalCACertificate(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteExternalCACertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActiveDirectoryRealmList - errors', () => {
      it('should have a getActiveDirectoryRealmList function', (done) => {
        try {
          assert.equal(true, typeof a.getActiveDirectoryRealmList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addActiveDirectoryRealm - errors', () => {
      it('should have a addActiveDirectoryRealm function', (done) => {
        try {
          assert.equal(true, typeof a.addActiveDirectoryRealm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addActiveDirectoryRealm(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addActiveDirectoryRealm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActiveDirectoryRealm - errors', () => {
      it('should have a getActiveDirectoryRealm function', (done) => {
        try {
          assert.equal(true, typeof a.getActiveDirectoryRealm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getActiveDirectoryRealm(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getActiveDirectoryRealm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editActiveDirectoryRealm - errors', () => {
      it('should have a editActiveDirectoryRealm function', (done) => {
        try {
          assert.equal(true, typeof a.editActiveDirectoryRealm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editActiveDirectoryRealm(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editActiveDirectoryRealm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editActiveDirectoryRealm('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editActiveDirectoryRealm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteActiveDirectoryRealm - errors', () => {
      it('should have a deleteActiveDirectoryRealm function', (done) => {
        try {
          assert.equal(true, typeof a.deleteActiveDirectoryRealm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteActiveDirectoryRealm(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteActiveDirectoryRealm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSpecialRealmList - errors', () => {
      it('should have a getSpecialRealmList function', (done) => {
        try {
          assert.equal(true, typeof a.getSpecialRealmList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSpecialRealm - errors', () => {
      it('should have a getSpecialRealm function', (done) => {
        try {
          assert.equal(true, typeof a.getSpecialRealm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getSpecialRealm(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSpecialRealm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRealmTrafficUserList - errors', () => {
      it('should have a getRealmTrafficUserList function', (done) => {
        try {
          assert.equal(true, typeof a.getRealmTrafficUserList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.getRealmTrafficUserList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getRealmTrafficUserList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrafficUserList - errors', () => {
      it('should have a getTrafficUserList function', (done) => {
        try {
          assert.equal(true, typeof a.getTrafficUserList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRealmTrafficUserGroupList - errors', () => {
      it('should have a getRealmTrafficUserGroupList function', (done) => {
        try {
          assert.equal(true, typeof a.getRealmTrafficUserGroupList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.getRealmTrafficUserGroupList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getRealmTrafficUserGroupList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrafficUserGroupList - errors', () => {
      it('should have a getTrafficUserGroupList function', (done) => {
        try {
          assert.equal(true, typeof a.getTrafficUserGroupList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConnectTest - errors', () => {
      it('should have a getConnectTest function', (done) => {
        try {
          assert.equal(true, typeof a.getConnectTest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getConnectTest(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getConnectTest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScheduleTroubleshootList - errors', () => {
      it('should have a getScheduleTroubleshootList function', (done) => {
        try {
          assert.equal(true, typeof a.getScheduleTroubleshootList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addScheduleTroubleshoot - errors', () => {
      it('should have a addScheduleTroubleshoot function', (done) => {
        try {
          assert.equal(true, typeof a.addScheduleTroubleshoot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addScheduleTroubleshoot(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addScheduleTroubleshoot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScheduleTroubleshoot - errors', () => {
      it('should have a getScheduleTroubleshoot function', (done) => {
        try {
          assert.equal(true, typeof a.getScheduleTroubleshoot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getScheduleTroubleshoot(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getScheduleTroubleshoot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editScheduleTroubleshoot - errors', () => {
      it('should have a editScheduleTroubleshoot function', (done) => {
        try {
          assert.equal(true, typeof a.editScheduleTroubleshoot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editScheduleTroubleshoot(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editScheduleTroubleshoot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editScheduleTroubleshoot('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editScheduleTroubleshoot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteScheduleTroubleshoot - errors', () => {
      it('should have a deleteScheduleTroubleshoot function', (done) => {
        try {
          assert.equal(true, typeof a.deleteScheduleTroubleshoot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteScheduleTroubleshoot(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteScheduleTroubleshoot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTroubleshootJobHistoryList - errors', () => {
      it('should have a getTroubleshootJobHistoryList function', (done) => {
        try {
          assert.equal(true, typeof a.getTroubleshootJobHistoryList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTroubleshootJobHistory - errors', () => {
      it('should have a getTroubleshootJobHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getTroubleshootJobHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getTroubleshootJobHistory(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getTroubleshootJobHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTroubleshootJobHistory - errors', () => {
      it('should have a deleteTroubleshootJobHistory function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTroubleshootJobHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteTroubleshootJobHistory(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteTroubleshootJobHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPullUpgradeImmediateList - errors', () => {
      it('should have a getPullUpgradeImmediateList function', (done) => {
        try {
          assert.equal(true, typeof a.getPullUpgradeImmediateList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPullUpgradeImmediate - errors', () => {
      it('should have a addPullUpgradeImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.addPullUpgradeImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addPullUpgradeImmediate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addPullUpgradeImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPullFileList - errors', () => {
      it('should have a getPullFileList function', (done) => {
        try {
          assert.equal(true, typeof a.getPullFileList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPullFile - errors', () => {
      it('should have a addPullFile function', (done) => {
        try {
          assert.equal(true, typeof a.addPullFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addPullFile(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addPullFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUpgradeFileList - errors', () => {
      it('should have a getUpgradeFileList function', (done) => {
        try {
          assert.equal(true, typeof a.getUpgradeFileList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUpgradeFile - errors', () => {
      it('should have a getUpgradeFile function', (done) => {
        try {
          assert.equal(true, typeof a.getUpgradeFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getUpgradeFile(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getUpgradeFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUpgradeFile - errors', () => {
      it('should have a deleteUpgradeFile function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUpgradeFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteUpgradeFile(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteUpgradeFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#startUpgrade - errors', () => {
      it('should have a startUpgrade function', (done) => {
        try {
          assert.equal(true, typeof a.startUpgrade === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addCancelUpgrade - errors', () => {
      it('should have a addCancelUpgrade function', (done) => {
        try {
          assert.equal(true, typeof a.addCancelUpgrade === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDHCPServerContainerList - errors', () => {
      it('should have a getDHCPServerContainerList function', (done) => {
        try {
          assert.equal(true, typeof a.getDHCPServerContainerList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDHCPServerContainer - errors', () => {
      it('should have a getDHCPServerContainer function', (done) => {
        try {
          assert.equal(true, typeof a.getDHCPServerContainer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getDHCPServerContainer(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getDHCPServerContainer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editDHCPServerContainer - errors', () => {
      it('should have a editDHCPServerContainer function', (done) => {
        try {
          assert.equal(true, typeof a.editDHCPServerContainer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editDHCPServerContainer(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editDHCPServerContainer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editDHCPServerContainer('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editDHCPServerContainer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudConfigList - errors', () => {
      it('should have a getCloudConfigList function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudConfigList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudConfig - errors', () => {
      it('should have a getCloudConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getCloudConfig(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getCloudConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editCloudConfig - errors', () => {
      it('should have a editCloudConfig function', (done) => {
        try {
          assert.equal(true, typeof a.editCloudConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editCloudConfig(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editCloudConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editCloudConfig('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editCloudConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postuploadcert - errors', () => {
      it('should have a postuploadcert function', (done) => {
        try {
          assert.equal(true, typeof a.postuploadcert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addTestDirectory - errors', () => {
      it('should have a addTestDirectory function', (done) => {
        try {
          assert.equal(true, typeof a.addTestDirectory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addTestDirectory(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addTestDirectory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addTestIdentityServicesEngineConnectivity - errors', () => {
      it('should have a addTestIdentityServicesEngineConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.addTestIdentityServicesEngineConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addTestIdentityServicesEngineConnectivity(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addTestIdentityServicesEngineConnectivity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityGroupTagList - errors', () => {
      it('should have a getSecurityGroupTagList function', (done) => {
        try {
          assert.equal(true, typeof a.getSecurityGroupTagList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityGroupTag - errors', () => {
      it('should have a getSecurityGroupTag function', (done) => {
        try {
          assert.equal(true, typeof a.getSecurityGroupTag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getSecurityGroupTag(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSecurityGroupTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSGTDynamicObjectList - errors', () => {
      it('should have a getSGTDynamicObjectList function', (done) => {
        try {
          assert.equal(true, typeof a.getSGTDynamicObjectList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSGTDynamicObject - errors', () => {
      it('should have a addSGTDynamicObject function', (done) => {
        try {
          assert.equal(true, typeof a.addSGTDynamicObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSGTDynamicObject(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addSGTDynamicObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSGTDynamicObject - errors', () => {
      it('should have a getSGTDynamicObject function', (done) => {
        try {
          assert.equal(true, typeof a.getSGTDynamicObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getSGTDynamicObject(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSGTDynamicObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editSGTDynamicObject - errors', () => {
      it('should have a editSGTDynamicObject function', (done) => {
        try {
          assert.equal(true, typeof a.editSGTDynamicObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editSGTDynamicObject(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSGTDynamicObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editSGTDynamicObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSGTDynamicObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSGTDynamicObject - errors', () => {
      it('should have a deleteSGTDynamicObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSGTDynamicObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteSGTDynamicObject(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteSGTDynamicObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNTPStatus - errors', () => {
      it('should have a getNTPStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getNTPStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getNTPStatus(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getNTPStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaceInfo - errors', () => {
      it('should have a getInterfaceInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getInterfaceInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getInterfaceInfo(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getInterfaceInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSSLPolicyList - errors', () => {
      it('should have a getSSLPolicyList function', (done) => {
        try {
          assert.equal(true, typeof a.getSSLPolicyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSSLPolicy - errors', () => {
      it('should have a getSSLPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getSSLPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getSSLPolicy(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSSLPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editSSLPolicy - errors', () => {
      it('should have a editSSLPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.editSSLPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editSSLPolicy(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSSLPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editSSLPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSSLPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSSLRuleList - errors', () => {
      it('should have a getSSLRuleList function', (done) => {
        try {
          assert.equal(true, typeof a.getSSLRuleList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.getSSLRuleList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSSLRuleList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSSLRule - errors', () => {
      it('should have a addSSLRule function', (done) => {
        try {
          assert.equal(true, typeof a.addSSLRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.addSSLRule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addSSLRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSSLRule('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addSSLRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSSLRule - errors', () => {
      it('should have a getSSLRule function', (done) => {
        try {
          assert.equal(true, typeof a.getSSLRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.getSSLRule(null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSSLRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getSSLRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSSLRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editSSLRule - errors', () => {
      it('should have a editSSLRule function', (done) => {
        try {
          assert.equal(true, typeof a.editSSLRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.editSSLRule('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSSLRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editSSLRule('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSSLRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editSSLRule('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSSLRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSSLRule - errors', () => {
      it('should have a deleteSSLRule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSSLRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.deleteSSLRule(null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteSSLRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteSSLRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteSSLRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostUpgradeFlagsList - errors', () => {
      it('should have a getPostUpgradeFlagsList function', (done) => {
        try {
          assert.equal(true, typeof a.getPostUpgradeFlagsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostUpgradeFlags - errors', () => {
      it('should have a getPostUpgradeFlags function', (done) => {
        try {
          assert.equal(true, typeof a.getPostUpgradeFlags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getPostUpgradeFlags(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getPostUpgradeFlags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editPostUpgradeFlags - errors', () => {
      it('should have a editPostUpgradeFlags function', (done) => {
        try {
          assert.equal(true, typeof a.editPostUpgradeFlags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editPostUpgradeFlags(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editPostUpgradeFlags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editPostUpgradeFlags('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editPostUpgradeFlags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOSPFList - errors', () => {
      it('should have a getOSPFList function', (done) => {
        try {
          assert.equal(true, typeof a.getOSPFList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfId', (done) => {
        try {
          a.getOSPFList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vrfId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getOSPFList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addOSPF - errors', () => {
      it('should have a addOSPF function', (done) => {
        try {
          assert.equal(true, typeof a.addOSPF === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfId', (done) => {
        try {
          a.addOSPF(null, null, (data, error) => {
            try {
              const displayE = 'vrfId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addOSPF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addOSPF('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addOSPF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOSPF - errors', () => {
      it('should have a getOSPF function', (done) => {
        try {
          assert.equal(true, typeof a.getOSPF === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfId', (done) => {
        try {
          a.getOSPF(null, null, (data, error) => {
            try {
              const displayE = 'vrfId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getOSPF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getOSPF('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getOSPF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editOSPF - errors', () => {
      it('should have a editOSPF function', (done) => {
        try {
          assert.equal(true, typeof a.editOSPF === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfId', (done) => {
        try {
          a.editOSPF(null, null, null, (data, error) => {
            try {
              const displayE = 'vrfId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editOSPF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editOSPF('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editOSPF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editOSPF('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editOSPF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOSPF - errors', () => {
      it('should have a deleteOSPF function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOSPF === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfId', (done) => {
        try {
          a.deleteOSPF(null, null, (data, error) => {
            try {
              const displayE = 'vrfId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteOSPF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteOSPF('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteOSPF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOSPFInterfaceSettingsList - errors', () => {
      it('should have a getOSPFInterfaceSettingsList function', (done) => {
        try {
          assert.equal(true, typeof a.getOSPFInterfaceSettingsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfId', (done) => {
        try {
          a.getOSPFInterfaceSettingsList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vrfId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getOSPFInterfaceSettingsList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addOSPFInterfaceSettings - errors', () => {
      it('should have a addOSPFInterfaceSettings function', (done) => {
        try {
          assert.equal(true, typeof a.addOSPFInterfaceSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfId', (done) => {
        try {
          a.addOSPFInterfaceSettings(null, null, (data, error) => {
            try {
              const displayE = 'vrfId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addOSPFInterfaceSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addOSPFInterfaceSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addOSPFInterfaceSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOSPFInterfaceSettings - errors', () => {
      it('should have a getOSPFInterfaceSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getOSPFInterfaceSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfId', (done) => {
        try {
          a.getOSPFInterfaceSettings(null, null, (data, error) => {
            try {
              const displayE = 'vrfId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getOSPFInterfaceSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getOSPFInterfaceSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getOSPFInterfaceSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editOSPFInterfaceSettings - errors', () => {
      it('should have a editOSPFInterfaceSettings function', (done) => {
        try {
          assert.equal(true, typeof a.editOSPFInterfaceSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfId', (done) => {
        try {
          a.editOSPFInterfaceSettings(null, null, null, (data, error) => {
            try {
              const displayE = 'vrfId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editOSPFInterfaceSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editOSPFInterfaceSettings('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editOSPFInterfaceSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editOSPFInterfaceSettings('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editOSPFInterfaceSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOSPFInterfaceSettings - errors', () => {
      it('should have a deleteOSPFInterfaceSettings function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOSPFInterfaceSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfId', (done) => {
        try {
          a.deleteOSPFInterfaceSettings(null, null, (data, error) => {
            try {
              const displayE = 'vrfId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteOSPFInterfaceSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteOSPFInterfaceSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteOSPFInterfaceSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBGPList - errors', () => {
      it('should have a getBGPList function', (done) => {
        try {
          assert.equal(true, typeof a.getBGPList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfId', (done) => {
        try {
          a.getBGPList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vrfId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getBGPList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addBGP - errors', () => {
      it('should have a addBGP function', (done) => {
        try {
          assert.equal(true, typeof a.addBGP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfId', (done) => {
        try {
          a.addBGP(null, null, (data, error) => {
            try {
              const displayE = 'vrfId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addBGP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addBGP('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addBGP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBGP - errors', () => {
      it('should have a getBGP function', (done) => {
        try {
          assert.equal(true, typeof a.getBGP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfId', (done) => {
        try {
          a.getBGP(null, null, (data, error) => {
            try {
              const displayE = 'vrfId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getBGP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getBGP('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getBGP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editBGP - errors', () => {
      it('should have a editBGP function', (done) => {
        try {
          assert.equal(true, typeof a.editBGP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfId', (done) => {
        try {
          a.editBGP(null, null, null, (data, error) => {
            try {
              const displayE = 'vrfId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editBGP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editBGP('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editBGP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editBGP('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editBGP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBGP - errors', () => {
      it('should have a deleteBGP function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBGP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vrfId', (done) => {
        try {
          a.deleteBGP(null, null, (data, error) => {
            try {
              const displayE = 'vrfId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteBGP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteBGP('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteBGP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBGPGeneralSettingsList - errors', () => {
      it('should have a getBGPGeneralSettingsList function', (done) => {
        try {
          assert.equal(true, typeof a.getBGPGeneralSettingsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addBGPGeneralSettings - errors', () => {
      it('should have a addBGPGeneralSettings function', (done) => {
        try {
          assert.equal(true, typeof a.addBGPGeneralSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addBGPGeneralSettings(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addBGPGeneralSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBGPGeneralSettings - errors', () => {
      it('should have a getBGPGeneralSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getBGPGeneralSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getBGPGeneralSettings(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getBGPGeneralSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editBGPGeneralSettings - errors', () => {
      it('should have a editBGPGeneralSettings function', (done) => {
        try {
          assert.equal(true, typeof a.editBGPGeneralSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editBGPGeneralSettings(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editBGPGeneralSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editBGPGeneralSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editBGPGeneralSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBGPGeneralSettings - errors', () => {
      it('should have a deleteBGPGeneralSettings function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBGPGeneralSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteBGPGeneralSettings(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteBGPGeneralSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlexConfigObjectList - errors', () => {
      it('should have a getFlexConfigObjectList function', (done) => {
        try {
          assert.equal(true, typeof a.getFlexConfigObjectList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addFlexConfigObject - errors', () => {
      it('should have a addFlexConfigObject function', (done) => {
        try {
          assert.equal(true, typeof a.addFlexConfigObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addFlexConfigObject(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addFlexConfigObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlexConfigObject - errors', () => {
      it('should have a getFlexConfigObject function', (done) => {
        try {
          assert.equal(true, typeof a.getFlexConfigObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getFlexConfigObject(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getFlexConfigObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editFlexConfigObject - errors', () => {
      it('should have a editFlexConfigObject function', (done) => {
        try {
          assert.equal(true, typeof a.editFlexConfigObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editFlexConfigObject(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editFlexConfigObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editFlexConfigObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editFlexConfigObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFlexConfigObject - errors', () => {
      it('should have a deleteFlexConfigObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFlexConfigObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteFlexConfigObject(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteFlexConfigObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlexConfigPolicyList - errors', () => {
      it('should have a getFlexConfigPolicyList function', (done) => {
        try {
          assert.equal(true, typeof a.getFlexConfigPolicyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addFlexConfigPolicy - errors', () => {
      it('should have a addFlexConfigPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.addFlexConfigPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addFlexConfigPolicy(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addFlexConfigPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlexConfigPolicy - errors', () => {
      it('should have a getFlexConfigPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getFlexConfigPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getFlexConfigPolicy(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getFlexConfigPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editFlexConfigPolicy - errors', () => {
      it('should have a editFlexConfigPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.editFlexConfigPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editFlexConfigPolicy(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editFlexConfigPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editFlexConfigPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editFlexConfigPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFlexConfigPolicy - errors', () => {
      it('should have a deleteFlexConfigPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFlexConfigPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteFlexConfigPolicy(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteFlexConfigPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHAConfigurationList - errors', () => {
      it('should have a getHAConfigurationList function', (done) => {
        try {
          assert.equal(true, typeof a.getHAConfigurationList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHAConfiguration - errors', () => {
      it('should have a getHAConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getHAConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getHAConfiguration(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getHAConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editHAConfiguration - errors', () => {
      it('should have a editHAConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.editHAConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editHAConfiguration(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editHAConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editHAConfiguration('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editHAConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHAFailoverConfigurationList - errors', () => {
      it('should have a getHAFailoverConfigurationList function', (done) => {
        try {
          assert.equal(true, typeof a.getHAFailoverConfigurationList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHAFailoverConfiguration - errors', () => {
      it('should have a getHAFailoverConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getHAFailoverConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getHAFailoverConfiguration(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getHAFailoverConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editHAFailoverConfiguration - errors', () => {
      it('should have a editHAFailoverConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.editHAFailoverConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editHAFailoverConfiguration(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editHAFailoverConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editHAFailoverConfiguration('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editHAFailoverConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHAStatus - errors', () => {
      it('should have a getHAStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getHAStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getHAStatus(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getHAStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRadiusIdentitySourceList - errors', () => {
      it('should have a getRadiusIdentitySourceList function', (done) => {
        try {
          assert.equal(true, typeof a.getRadiusIdentitySourceList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addRadiusIdentitySource - errors', () => {
      it('should have a addRadiusIdentitySource function', (done) => {
        try {
          assert.equal(true, typeof a.addRadiusIdentitySource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addRadiusIdentitySource(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addRadiusIdentitySource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRadiusIdentitySource - errors', () => {
      it('should have a getRadiusIdentitySource function', (done) => {
        try {
          assert.equal(true, typeof a.getRadiusIdentitySource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getRadiusIdentitySource(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getRadiusIdentitySource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editRadiusIdentitySource - errors', () => {
      it('should have a editRadiusIdentitySource function', (done) => {
        try {
          assert.equal(true, typeof a.editRadiusIdentitySource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editRadiusIdentitySource(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editRadiusIdentitySource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editRadiusIdentitySource('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editRadiusIdentitySource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRadiusIdentitySource - errors', () => {
      it('should have a deleteRadiusIdentitySource function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRadiusIdentitySource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteRadiusIdentitySource(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteRadiusIdentitySource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLocalIdentitySourceList - errors', () => {
      it('should have a getLocalIdentitySourceList function', (done) => {
        try {
          assert.equal(true, typeof a.getLocalIdentitySourceList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLocalIdentitySource - errors', () => {
      it('should have a getLocalIdentitySource function', (done) => {
        try {
          assert.equal(true, typeof a.getLocalIdentitySource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getLocalIdentitySource(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getLocalIdentitySource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addTestIdentitySource - errors', () => {
      it('should have a addTestIdentitySource function', (done) => {
        try {
          assert.equal(true, typeof a.addTestIdentitySource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addTestIdentitySource(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addTestIdentitySource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActiveUserSessionsList - errors', () => {
      it('should have a getActiveUserSessionsList function', (done) => {
        try {
          assert.equal(true, typeof a.getActiveUserSessionsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActiveUserSessions - errors', () => {
      it('should have a getActiveUserSessions function', (done) => {
        try {
          assert.equal(true, typeof a.getActiveUserSessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getActiveUserSessions(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getActiveUserSessions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteActiveUserSessions - errors', () => {
      it('should have a deleteActiveUserSessions function', (done) => {
        try {
          assert.equal(true, typeof a.deleteActiveUserSessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteActiveUserSessions(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteActiveUserSessions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAAASettingList - errors', () => {
      it('should have a getAAASettingList function', (done) => {
        try {
          assert.equal(true, typeof a.getAAASettingList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAAASetting - errors', () => {
      it('should have a getAAASetting function', (done) => {
        try {
          assert.equal(true, typeof a.getAAASetting === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getAAASetting(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getAAASetting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editAAASetting - errors', () => {
      it('should have a editAAASetting function', (done) => {
        try {
          assert.equal(true, typeof a.editAAASetting === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editAAASetting(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editAAASetting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editAAASetting('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editAAASetting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRadiusIdentitySourceGroupList - errors', () => {
      it('should have a getRadiusIdentitySourceGroupList function', (done) => {
        try {
          assert.equal(true, typeof a.getRadiusIdentitySourceGroupList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addRadiusIdentitySourceGroup - errors', () => {
      it('should have a addRadiusIdentitySourceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.addRadiusIdentitySourceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addRadiusIdentitySourceGroup(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addRadiusIdentitySourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRadiusIdentitySourceGroup - errors', () => {
      it('should have a getRadiusIdentitySourceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getRadiusIdentitySourceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getRadiusIdentitySourceGroup(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getRadiusIdentitySourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editRadiusIdentitySourceGroup - errors', () => {
      it('should have a editRadiusIdentitySourceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.editRadiusIdentitySourceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editRadiusIdentitySourceGroup(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editRadiusIdentitySourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editRadiusIdentitySourceGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editRadiusIdentitySourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRadiusIdentitySourceGroup - errors', () => {
      it('should have a deleteRadiusIdentitySourceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRadiusIdentitySourceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteRadiusIdentitySourceGroup(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteRadiusIdentitySourceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRolePermissionList - errors', () => {
      it('should have a getRolePermissionList function', (done) => {
        try {
          assert.equal(true, typeof a.getRolePermissionList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRolePermission - errors', () => {
      it('should have a getRolePermission function', (done) => {
        try {
          assert.equal(true, typeof a.getRolePermission === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getRolePermission(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getRolePermission', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomLoggingListList - errors', () => {
      it('should have a getCustomLoggingListList function', (done) => {
        try {
          assert.equal(true, typeof a.getCustomLoggingListList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addCustomLoggingList - errors', () => {
      it('should have a addCustomLoggingList function', (done) => {
        try {
          assert.equal(true, typeof a.addCustomLoggingList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addCustomLoggingList(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addCustomLoggingList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomLoggingList - errors', () => {
      it('should have a getCustomLoggingList function', (done) => {
        try {
          assert.equal(true, typeof a.getCustomLoggingList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getCustomLoggingList(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getCustomLoggingList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editCustomLoggingList - errors', () => {
      it('should have a editCustomLoggingList function', (done) => {
        try {
          assert.equal(true, typeof a.editCustomLoggingList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editCustomLoggingList(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editCustomLoggingList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editCustomLoggingList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editCustomLoggingList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCustomLoggingList - errors', () => {
      it('should have a deleteCustomLoggingList function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCustomLoggingList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteCustomLoggingList(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteCustomLoggingList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebAnalyticsSettingList - errors', () => {
      it('should have a getWebAnalyticsSettingList function', (done) => {
        try {
          assert.equal(true, typeof a.getWebAnalyticsSettingList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebAnalyticsSetting - errors', () => {
      it('should have a getWebAnalyticsSetting function', (done) => {
        try {
          assert.equal(true, typeof a.getWebAnalyticsSetting === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getWebAnalyticsSetting(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getWebAnalyticsSetting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editWebAnalyticsSetting - errors', () => {
      it('should have a editWebAnalyticsSetting function', (done) => {
        try {
          assert.equal(true, typeof a.editWebAnalyticsSetting === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editWebAnalyticsSetting(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editWebAnalyticsSetting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editWebAnalyticsSetting('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editWebAnalyticsSetting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInitialProvisionList - errors', () => {
      it('should have a getInitialProvisionList function', (done) => {
        try {
          assert.equal(true, typeof a.getInitialProvisionList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addInitialProvision - errors', () => {
      it('should have a addInitialProvision function', (done) => {
        try {
          assert.equal(true, typeof a.addInitialProvision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addInitialProvision(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addInitialProvision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInitialProvision - errors', () => {
      it('should have a getInitialProvision function', (done) => {
        try {
          assert.equal(true, typeof a.getInitialProvision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getInitialProvision(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getInitialProvision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHitCountList - errors', () => {
      it('should have a getHitCountList function', (done) => {
        try {
          assert.equal(true, typeof a.getHitCountList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.getHitCountList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getHitCountList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editHitCount - errors', () => {
      it('should have a editHitCount function', (done) => {
        try {
          assert.equal(true, typeof a.editHitCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.editHitCount(null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editHitCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHitCount - errors', () => {
      it('should have a deleteHitCount function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHitCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.deleteHitCount('fakeparam', null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteHitCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDuoLDAPIdentitySourceList - errors', () => {
      it('should have a getDuoLDAPIdentitySourceList function', (done) => {
        try {
          assert.equal(true, typeof a.getDuoLDAPIdentitySourceList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addDuoLDAPIdentitySource - errors', () => {
      it('should have a addDuoLDAPIdentitySource function', (done) => {
        try {
          assert.equal(true, typeof a.addDuoLDAPIdentitySource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addDuoLDAPIdentitySource(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addDuoLDAPIdentitySource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDuoLDAPIdentitySource - errors', () => {
      it('should have a getDuoLDAPIdentitySource function', (done) => {
        try {
          assert.equal(true, typeof a.getDuoLDAPIdentitySource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getDuoLDAPIdentitySource(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getDuoLDAPIdentitySource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editDuoLDAPIdentitySource - errors', () => {
      it('should have a editDuoLDAPIdentitySource function', (done) => {
        try {
          assert.equal(true, typeof a.editDuoLDAPIdentitySource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editDuoLDAPIdentitySource(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editDuoLDAPIdentitySource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editDuoLDAPIdentitySource('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editDuoLDAPIdentitySource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDuoLDAPIdentitySource - errors', () => {
      it('should have a deleteDuoLDAPIdentitySource function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDuoLDAPIdentitySource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteDuoLDAPIdentitySource(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteDuoLDAPIdentitySource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFilePolicyList - errors', () => {
      it('should have a getFilePolicyList function', (done) => {
        try {
          assert.equal(true, typeof a.getFilePolicyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addFilePolicy - errors', () => {
      it('should have a addFilePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.addFilePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addFilePolicy(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addFilePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFilePolicy - errors', () => {
      it('should have a getFilePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getFilePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getFilePolicy(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getFilePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editFilePolicy - errors', () => {
      it('should have a editFilePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.editFilePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editFilePolicy(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editFilePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editFilePolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editFilePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFilePolicy - errors', () => {
      it('should have a deleteFilePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFilePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteFilePolicy(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteFilePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFilePolicyConfigurationList - errors', () => {
      it('should have a getFilePolicyConfigurationList function', (done) => {
        try {
          assert.equal(true, typeof a.getFilePolicyConfigurationList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFilePolicyConfiguration - errors', () => {
      it('should have a getFilePolicyConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getFilePolicyConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getFilePolicyConfiguration(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getFilePolicyConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editFilePolicyConfiguration - errors', () => {
      it('should have a editFilePolicyConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.editFilePolicyConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editFilePolicyConfiguration(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editFilePolicyConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editFilePolicyConfiguration('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editFilePolicyConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFileRuleList - errors', () => {
      it('should have a getFileRuleList function', (done) => {
        try {
          assert.equal(true, typeof a.getFileRuleList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.getFileRuleList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getFileRuleList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addFileRule - errors', () => {
      it('should have a addFileRule function', (done) => {
        try {
          assert.equal(true, typeof a.addFileRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.addFileRule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addFileRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addFileRule('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addFileRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFileRule - errors', () => {
      it('should have a getFileRule function', (done) => {
        try {
          assert.equal(true, typeof a.getFileRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.getFileRule(null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getFileRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getFileRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getFileRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editFileRule - errors', () => {
      it('should have a editFileRule function', (done) => {
        try {
          assert.equal(true, typeof a.editFileRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.editFileRule('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editFileRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editFileRule('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editFileRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editFileRule('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editFileRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFileRule - errors', () => {
      it('should have a deleteFileRule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFileRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.deleteFileRule(null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteFileRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteFileRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteFileRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFileTypeList - errors', () => {
      it('should have a getFileTypeList function', (done) => {
        try {
          assert.equal(true, typeof a.getFileTypeList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFileType - errors', () => {
      it('should have a getFileType function', (done) => {
        try {
          assert.equal(true, typeof a.getFileType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getFileType(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getFileType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFileTypeCategoryList - errors', () => {
      it('should have a getFileTypeCategoryList function', (done) => {
        try {
          assert.equal(true, typeof a.getFileTypeCategoryList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFileTypeCategory - errors', () => {
      it('should have a getFileTypeCategory function', (done) => {
        try {
          assert.equal(true, typeof a.getFileTypeCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getFileTypeCategory(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getFileTypeCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAmpCloudConfigList - errors', () => {
      it('should have a getAmpCloudConfigList function', (done) => {
        try {
          assert.equal(true, typeof a.getAmpCloudConfigList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAmpCloudConfig - errors', () => {
      it('should have a getAmpCloudConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getAmpCloudConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getAmpCloudConfig(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getAmpCloudConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editAmpCloudConfig - errors', () => {
      it('should have a editAmpCloudConfig function', (done) => {
        try {
          assert.equal(true, typeof a.editAmpCloudConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editAmpCloudConfig(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editAmpCloudConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editAmpCloudConfig('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editAmpCloudConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAMPServerList - errors', () => {
      it('should have a getAMPServerList function', (done) => {
        try {
          assert.equal(true, typeof a.getAMPServerList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAMPServer - errors', () => {
      it('should have a getAMPServer function', (done) => {
        try {
          assert.equal(true, typeof a.getAMPServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getAMPServer(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getAMPServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAMPCloudConnectionList - errors', () => {
      it('should have a getAMPCloudConnectionList function', (done) => {
        try {
          assert.equal(true, typeof a.getAMPCloudConnectionList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAMPCloudConnection - errors', () => {
      it('should have a getAMPCloudConnection function', (done) => {
        try {
          assert.equal(true, typeof a.getAMPCloudConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getAMPCloudConnection(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getAMPCloudConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editAMPCloudConnection - errors', () => {
      it('should have a editAMPCloudConnection function', (done) => {
        try {
          assert.equal(true, typeof a.editAMPCloudConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editAMPCloudConnection(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editAMPCloudConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editAMPCloudConnection('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editAMPCloudConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getdownloadstoredfileshalist - errors', () => {
      it('should have a getdownloadstoredfileshalist function', (done) => {
        try {
          assert.equal(true, typeof a.getdownloadstoredfileshalist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getdownloadstoredfileshalist(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getdownloadstoredfileshalist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScheduleStoredFileSHAListList - errors', () => {
      it('should have a getScheduleStoredFileSHAListList function', (done) => {
        try {
          assert.equal(true, typeof a.getScheduleStoredFileSHAListList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addScheduleStoredFileSHAList - errors', () => {
      it('should have a addScheduleStoredFileSHAList function', (done) => {
        try {
          assert.equal(true, typeof a.addScheduleStoredFileSHAList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addScheduleStoredFileSHAList(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addScheduleStoredFileSHAList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScheduleStoredFileSHAListJobHistoryList - errors', () => {
      it('should have a getScheduleStoredFileSHAListJobHistoryList function', (done) => {
        try {
          assert.equal(true, typeof a.getScheduleStoredFileSHAListJobHistoryList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getdownloadstoredfiles - errors', () => {
      it('should have a getdownloadstoredfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getdownloadstoredfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getdownloadstoredfiles(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getdownloadstoredfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postuploadcleanhashlist - errors', () => {
      it('should have a postuploadcleanhashlist function', (done) => {
        try {
          assert.equal(true, typeof a.postuploadcleanhashlist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getdownloadcleanhashlist - errors', () => {
      it('should have a getdownloadcleanhashlist function', (done) => {
        try {
          assert.equal(true, typeof a.getdownloadcleanhashlist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getdownloadcleanhashlist(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getdownloadcleanhashlist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCleanListList - errors', () => {
      it('should have a getCleanListList function', (done) => {
        try {
          assert.equal(true, typeof a.getCleanListList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCleanList - errors', () => {
      it('should have a getCleanList function', (done) => {
        try {
          assert.equal(true, typeof a.getCleanList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getCleanList(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getCleanList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCleanList - errors', () => {
      it('should have a deleteCleanList function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCleanList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteCleanList(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteCleanList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postuploadcustomdetectionhashlist - errors', () => {
      it('should have a postuploadcustomdetectionhashlist function', (done) => {
        try {
          assert.equal(true, typeof a.postuploadcustomdetectionhashlist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getdownloadcustomdetectionhashlist - errors', () => {
      it('should have a getdownloadcustomdetectionhashlist function', (done) => {
        try {
          assert.equal(true, typeof a.getdownloadcustomdetectionhashlist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getdownloadcustomdetectionhashlist(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getdownloadcustomdetectionhashlist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomDetectionListList - errors', () => {
      it('should have a getCustomDetectionListList function', (done) => {
        try {
          assert.equal(true, typeof a.getCustomDetectionListList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomDetectionList - errors', () => {
      it('should have a getCustomDetectionList function', (done) => {
        try {
          assert.equal(true, typeof a.getCustomDetectionList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getCustomDetectionList(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getCustomDetectionList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCustomDetectionList - errors', () => {
      it('should have a deleteCustomDetectionList function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCustomDetectionList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteCustomDetectionList(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteCustomDetectionList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMalwareUpdateConnectionStatusList - errors', () => {
      it('should have a getMalwareUpdateConnectionStatusList function', (done) => {
        try {
          assert.equal(true, typeof a.getMalwareUpdateConnectionStatusList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAMPCloudConnectionStatusList - errors', () => {
      it('should have a getAMPCloudConnectionStatusList function', (done) => {
        try {
          assert.equal(true, typeof a.getAMPCloudConnectionStatusList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualRouterList - errors', () => {
      it('should have a getVirtualRouterList function', (done) => {
        try {
          assert.equal(true, typeof a.getVirtualRouterList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addVirtualRouter - errors', () => {
      it('should have a addVirtualRouter function', (done) => {
        try {
          assert.equal(true, typeof a.addVirtualRouter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addVirtualRouter(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addVirtualRouter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualRouter - errors', () => {
      it('should have a getVirtualRouter function', (done) => {
        try {
          assert.equal(true, typeof a.getVirtualRouter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getVirtualRouter(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getVirtualRouter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editVirtualRouter - errors', () => {
      it('should have a editVirtualRouter function', (done) => {
        try {
          assert.equal(true, typeof a.editVirtualRouter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editVirtualRouter(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editVirtualRouter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editVirtualRouter('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editVirtualRouter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualRouter - errors', () => {
      it('should have a deleteVirtualRouter function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVirtualRouter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteVirtualRouter(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteVirtualRouter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStaticRouteEntryList - errors', () => {
      it('should have a getStaticRouteEntryList function', (done) => {
        try {
          assert.equal(true, typeof a.getStaticRouteEntryList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.getStaticRouteEntryList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getStaticRouteEntryList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addStaticRouteEntry - errors', () => {
      it('should have a addStaticRouteEntry function', (done) => {
        try {
          assert.equal(true, typeof a.addStaticRouteEntry === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.addStaticRouteEntry('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addStaticRouteEntry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addStaticRouteEntry('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addStaticRouteEntry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStaticRouteEntry - errors', () => {
      it('should have a getStaticRouteEntry function', (done) => {
        try {
          assert.equal(true, typeof a.getStaticRouteEntry === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.getStaticRouteEntry(null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getStaticRouteEntry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getStaticRouteEntry('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getStaticRouteEntry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editStaticRouteEntry - errors', () => {
      it('should have a editStaticRouteEntry function', (done) => {
        try {
          assert.equal(true, typeof a.editStaticRouteEntry === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.editStaticRouteEntry('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editStaticRouteEntry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editStaticRouteEntry('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editStaticRouteEntry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editStaticRouteEntry('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editStaticRouteEntry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteStaticRouteEntry - errors', () => {
      it('should have a deleteStaticRouteEntry function', (done) => {
        try {
          assert.equal(true, typeof a.deleteStaticRouteEntry === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.deleteStaticRouteEntry(null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteStaticRouteEntry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteStaticRouteEntry('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteStaticRouteEntry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScheduleConfigExportList - errors', () => {
      it('should have a getScheduleConfigExportList function', (done) => {
        try {
          assert.equal(true, typeof a.getScheduleConfigExportList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addScheduleConfigExport - errors', () => {
      it('should have a addScheduleConfigExport function', (done) => {
        try {
          assert.equal(true, typeof a.addScheduleConfigExport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addScheduleConfigExport(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addScheduleConfigExport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigExportJobStatus - errors', () => {
      it('should have a getConfigExportJobStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getConfigExportJobStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getConfigExportJobStatus(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getConfigExportJobStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteConfigExportJobStatus - errors', () => {
      it('should have a deleteConfigExportJobStatus function', (done) => {
        try {
          assert.equal(true, typeof a.deleteConfigExportJobStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteConfigExportJobStatus(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteConfigExportJobStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigExportJobStatusList - errors', () => {
      it('should have a getConfigExportJobStatusList function', (done) => {
        try {
          assert.equal(true, typeof a.getConfigExportJobStatusList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadConfigFile - errors', () => {
      it('should have a downloadConfigFile function', (done) => {
        try {
          assert.equal(true, typeof a.downloadConfigFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.downloadConfigFile(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-downloadConfigFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postuploadconfigfile - errors', () => {
      it('should have a postuploadconfigfile function', (done) => {
        try {
          assert.equal(true, typeof a.postuploadconfigfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigImportExportFileInfo - errors', () => {
      it('should have a getConfigImportExportFileInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getConfigImportExportFileInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getConfigImportExportFileInfo(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getConfigImportExportFileInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteConfigImportExportFileInfo - errors', () => {
      it('should have a deleteConfigImportExportFileInfo function', (done) => {
        try {
          assert.equal(true, typeof a.deleteConfigImportExportFileInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteConfigImportExportFileInfo(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteConfigImportExportFileInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigImportExportFileInfoList - errors', () => {
      it('should have a getConfigImportExportFileInfoList function', (done) => {
        try {
          assert.equal(true, typeof a.getConfigImportExportFileInfoList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScheduleConfigImportList - errors', () => {
      it('should have a getScheduleConfigImportList function', (done) => {
        try {
          assert.equal(true, typeof a.getScheduleConfigImportList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addScheduleConfigImport - errors', () => {
      it('should have a addScheduleConfigImport function', (done) => {
        try {
          assert.equal(true, typeof a.addScheduleConfigImport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addScheduleConfigImport(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addScheduleConfigImport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigImportJobStatus - errors', () => {
      it('should have a getConfigImportJobStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getConfigImportJobStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getConfigImportJobStatus(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getConfigImportJobStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteConfigImportJobStatus - errors', () => {
      it('should have a deleteConfigImportJobStatus function', (done) => {
        try {
          assert.equal(true, typeof a.deleteConfigImportJobStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteConfigImportJobStatus(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteConfigImportJobStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigImportJobStatusList - errors', () => {
      it('should have a getConfigImportJobStatusList function', (done) => {
        try {
          assert.equal(true, typeof a.getConfigImportJobStatusList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntrusionPolicyList - errors', () => {
      it('should have a getIntrusionPolicyList function', (done) => {
        try {
          assert.equal(true, typeof a.getIntrusionPolicyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntrusionPolicy - errors', () => {
      it('should have a getIntrusionPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getIntrusionPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getIntrusionPolicy(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getIntrusionPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editIntrusionPolicy - errors', () => {
      it('should have a editIntrusionPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.editIntrusionPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editIntrusionPolicy(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editIntrusionPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editIntrusionPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editIntrusionPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntrusionRuleList - errors', () => {
      it('should have a getIntrusionRuleList function', (done) => {
        try {
          assert.equal(true, typeof a.getIntrusionRuleList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.getIntrusionRuleList('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getIntrusionRuleList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntrusionRule - errors', () => {
      it('should have a getIntrusionRule function', (done) => {
        try {
          assert.equal(true, typeof a.getIntrusionRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.getIntrusionRule(null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getIntrusionRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getIntrusionRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getIntrusionRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editIntrusionPolicyRuleUpdate - errors', () => {
      it('should have a editIntrusionPolicyRuleUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.editIntrusionPolicyRuleUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editIntrusionPolicyRuleUpdate(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editIntrusionPolicyRuleUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editIntrusionPolicyRuleUpdate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editIntrusionPolicyRuleUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntrusionSettingsList - errors', () => {
      it('should have a getIntrusionSettingsList function', (done) => {
        try {
          assert.equal(true, typeof a.getIntrusionSettingsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntrusionSettings - errors', () => {
      it('should have a getIntrusionSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getIntrusionSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getIntrusionSettings(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getIntrusionSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editIntrusionSettings - errors', () => {
      it('should have a editIntrusionSettings function', (done) => {
        try {
          assert.equal(true, typeof a.editIntrusionSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editIntrusionSettings(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editIntrusionSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editIntrusionSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editIntrusionSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDiskUsage - errors', () => {
      it('should have a getDiskUsage function', (done) => {
        try {
          assert.equal(true, typeof a.getDiskUsage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getDiskUsage(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getDiskUsage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrendingReport - errors', () => {
      it('should have a getTrendingReport function', (done) => {
        try {
          assert.equal(true, typeof a.getTrendingReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getTrendingReport('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getTrendingReport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSLAMonitorList - errors', () => {
      it('should have a getSLAMonitorList function', (done) => {
        try {
          assert.equal(true, typeof a.getSLAMonitorList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSLAMonitor - errors', () => {
      it('should have a addSLAMonitor function', (done) => {
        try {
          assert.equal(true, typeof a.addSLAMonitor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSLAMonitor(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addSLAMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSLAMonitor - errors', () => {
      it('should have a getSLAMonitor function', (done) => {
        try {
          assert.equal(true, typeof a.getSLAMonitor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getSLAMonitor(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSLAMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editSLAMonitor - errors', () => {
      it('should have a editSLAMonitor function', (done) => {
        try {
          assert.equal(true, typeof a.editSLAMonitor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editSLAMonitor(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSLAMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editSLAMonitor('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSLAMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSLAMonitor - errors', () => {
      it('should have a deleteSLAMonitor function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSLAMonitor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteSLAMonitor(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteSLAMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSLAMonitorStatus - errors', () => {
      it('should have a getSLAMonitorStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getSLAMonitorStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getSLAMonitorStatus(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSLAMonitorStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSLAMonitorStatusList - errors', () => {
      it('should have a getSLAMonitorStatusList function', (done) => {
        try {
          assert.equal(true, typeof a.getSLAMonitorStatusList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityIntelligenceUpdateFeedsImmediateList - errors', () => {
      it('should have a getSecurityIntelligenceUpdateFeedsImmediateList function', (done) => {
        try {
          assert.equal(true, typeof a.getSecurityIntelligenceUpdateFeedsImmediateList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSecurityIntelligenceUpdateFeedsImmediate - errors', () => {
      it('should have a addSecurityIntelligenceUpdateFeedsImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.addSecurityIntelligenceUpdateFeedsImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSecurityIntelligenceUpdateFeedsImmediate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addSecurityIntelligenceUpdateFeedsImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityIntelligenceUpdateFeedsImmediate - errors', () => {
      it('should have a getSecurityIntelligenceUpdateFeedsImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.getSecurityIntelligenceUpdateFeedsImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getSecurityIntelligenceUpdateFeedsImmediate(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSecurityIntelligenceUpdateFeedsImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editSecurityIntelligenceUpdateFeedsImmediate - errors', () => {
      it('should have a editSecurityIntelligenceUpdateFeedsImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.editSecurityIntelligenceUpdateFeedsImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editSecurityIntelligenceUpdateFeedsImmediate(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSecurityIntelligenceUpdateFeedsImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editSecurityIntelligenceUpdateFeedsImmediate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSecurityIntelligenceUpdateFeedsImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecurityIntelligenceUpdateFeedsImmediate - errors', () => {
      it('should have a deleteSecurityIntelligenceUpdateFeedsImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecurityIntelligenceUpdateFeedsImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteSecurityIntelligenceUpdateFeedsImmediate(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteSecurityIntelligenceUpdateFeedsImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityIntelligenceUpdateFeedsScheduleList - errors', () => {
      it('should have a getSecurityIntelligenceUpdateFeedsScheduleList function', (done) => {
        try {
          assert.equal(true, typeof a.getSecurityIntelligenceUpdateFeedsScheduleList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSecurityIntelligenceUpdateFeedsSchedule - errors', () => {
      it('should have a addSecurityIntelligenceUpdateFeedsSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.addSecurityIntelligenceUpdateFeedsSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSecurityIntelligenceUpdateFeedsSchedule(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addSecurityIntelligenceUpdateFeedsSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityIntelligenceUpdateFeedsSchedule - errors', () => {
      it('should have a getSecurityIntelligenceUpdateFeedsSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.getSecurityIntelligenceUpdateFeedsSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getSecurityIntelligenceUpdateFeedsSchedule(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSecurityIntelligenceUpdateFeedsSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editSecurityIntelligenceUpdateFeedsSchedule - errors', () => {
      it('should have a editSecurityIntelligenceUpdateFeedsSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.editSecurityIntelligenceUpdateFeedsSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editSecurityIntelligenceUpdateFeedsSchedule(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSecurityIntelligenceUpdateFeedsSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editSecurityIntelligenceUpdateFeedsSchedule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSecurityIntelligenceUpdateFeedsSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecurityIntelligenceUpdateFeedsSchedule - errors', () => {
      it('should have a deleteSecurityIntelligenceUpdateFeedsSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecurityIntelligenceUpdateFeedsSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteSecurityIntelligenceUpdateFeedsSchedule(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteSecurityIntelligenceUpdateFeedsSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDomainNameFeedList - errors', () => {
      it('should have a getDomainNameFeedList function', (done) => {
        try {
          assert.equal(true, typeof a.getDomainNameFeedList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addDomainNameFeed - errors', () => {
      it('should have a addDomainNameFeed function', (done) => {
        try {
          assert.equal(true, typeof a.addDomainNameFeed === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addDomainNameFeed(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addDomainNameFeed', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDomainNameFeed - errors', () => {
      it('should have a getDomainNameFeed function', (done) => {
        try {
          assert.equal(true, typeof a.getDomainNameFeed === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getDomainNameFeed(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getDomainNameFeed', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editDomainNameFeed - errors', () => {
      it('should have a editDomainNameFeed function', (done) => {
        try {
          assert.equal(true, typeof a.editDomainNameFeed === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editDomainNameFeed(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editDomainNameFeed', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editDomainNameFeed('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editDomainNameFeed', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDomainNameFeed - errors', () => {
      it('should have a deleteDomainNameFeed function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDomainNameFeed === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteDomainNameFeed(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteDomainNameFeed', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDomainNameGroupList - errors', () => {
      it('should have a getDomainNameGroupList function', (done) => {
        try {
          assert.equal(true, typeof a.getDomainNameGroupList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDomainNameGroup - errors', () => {
      it('should have a getDomainNameGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getDomainNameGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getDomainNameGroup(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getDomainNameGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editDomainNameGroup - errors', () => {
      it('should have a editDomainNameGroup function', (done) => {
        try {
          assert.equal(true, typeof a.editDomainNameGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editDomainNameGroup(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editDomainNameGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editDomainNameGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editDomainNameGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDomainNameFeedCategoryList - errors', () => {
      it('should have a getDomainNameFeedCategoryList function', (done) => {
        try {
          assert.equal(true, typeof a.getDomainNameFeedCategoryList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDomainNameFeedCategory - errors', () => {
      it('should have a getDomainNameFeedCategory function', (done) => {
        try {
          assert.equal(true, typeof a.getDomainNameFeedCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getDomainNameFeedCategory(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getDomainNameFeedCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemFeedObjectList - errors', () => {
      it('should have a getSystemFeedObjectList function', (done) => {
        try {
          assert.equal(true, typeof a.getSystemFeedObjectList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemFeedObject - errors', () => {
      it('should have a getSystemFeedObject function', (done) => {
        try {
          assert.equal(true, typeof a.getSystemFeedObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getSystemFeedObject(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSystemFeedObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editSystemFeedObject - errors', () => {
      it('should have a editSystemFeedObject function', (done) => {
        try {
          assert.equal(true, typeof a.editSystemFeedObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editSystemFeedObject(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSystemFeedObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editSystemFeedObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSystemFeedObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getURLFeedCategoryList - errors', () => {
      it('should have a getURLFeedCategoryList function', (done) => {
        try {
          assert.equal(true, typeof a.getURLFeedCategoryList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getURLFeedCategory - errors', () => {
      it('should have a getURLFeedCategory function', (done) => {
        try {
          assert.equal(true, typeof a.getURLFeedCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getURLFeedCategory(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getURLFeedCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkFeedCategoryList - errors', () => {
      it('should have a getNetworkFeedCategoryList function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkFeedCategoryList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkFeedCategory - errors', () => {
      it('should have a getNetworkFeedCategory function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkFeedCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getNetworkFeedCategory(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getNetworkFeedCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityIntelligencePolicyList - errors', () => {
      it('should have a getSecurityIntelligencePolicyList function', (done) => {
        try {
          assert.equal(true, typeof a.getSecurityIntelligencePolicyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityIntelligencePolicy - errors', () => {
      it('should have a getSecurityIntelligencePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getSecurityIntelligencePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getSecurityIntelligencePolicy(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSecurityIntelligencePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editSecurityIntelligencePolicy - errors', () => {
      it('should have a editSecurityIntelligencePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.editSecurityIntelligencePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editSecurityIntelligencePolicy(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSecurityIntelligencePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editSecurityIntelligencePolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSecurityIntelligencePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityIntelligenceNetworkPolicyList - errors', () => {
      it('should have a getSecurityIntelligenceNetworkPolicyList function', (done) => {
        try {
          assert.equal(true, typeof a.getSecurityIntelligenceNetworkPolicyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityIntelligenceNetworkPolicy - errors', () => {
      it('should have a getSecurityIntelligenceNetworkPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getSecurityIntelligenceNetworkPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getSecurityIntelligenceNetworkPolicy(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSecurityIntelligenceNetworkPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editSecurityIntelligenceNetworkPolicy - errors', () => {
      it('should have a editSecurityIntelligenceNetworkPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.editSecurityIntelligenceNetworkPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editSecurityIntelligenceNetworkPolicy(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSecurityIntelligenceNetworkPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editSecurityIntelligenceNetworkPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSecurityIntelligenceNetworkPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityIntelligenceURLPolicyList - errors', () => {
      it('should have a getSecurityIntelligenceURLPolicyList function', (done) => {
        try {
          assert.equal(true, typeof a.getSecurityIntelligenceURLPolicyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityIntelligenceURLPolicy - errors', () => {
      it('should have a getSecurityIntelligenceURLPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getSecurityIntelligenceURLPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getSecurityIntelligenceURLPolicy(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSecurityIntelligenceURLPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editSecurityIntelligenceURLPolicy - errors', () => {
      it('should have a editSecurityIntelligenceURLPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.editSecurityIntelligenceURLPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editSecurityIntelligenceURLPolicy(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSecurityIntelligenceURLPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editSecurityIntelligenceURLPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSecurityIntelligenceURLPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityIntelligenceDNSPolicyList - errors', () => {
      it('should have a getSecurityIntelligenceDNSPolicyList function', (done) => {
        try {
          assert.equal(true, typeof a.getSecurityIntelligenceDNSPolicyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityIntelligenceDNSPolicy - errors', () => {
      it('should have a getSecurityIntelligenceDNSPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getSecurityIntelligenceDNSPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getSecurityIntelligenceDNSPolicy(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSecurityIntelligenceDNSPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editSecurityIntelligenceDNSPolicy - errors', () => {
      it('should have a editSecurityIntelligenceDNSPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.editSecurityIntelligenceDNSPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editSecurityIntelligenceDNSPolicy(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSecurityIntelligenceDNSPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editSecurityIntelligenceDNSPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSecurityIntelligenceDNSPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceMetricDataList - errors', () => {
      it('should have a getDeviceMetricDataList function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceMetricDataList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceMetricData - errors', () => {
      it('should have a getDeviceMetricData function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceMetricData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getDeviceMetricData(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getDeviceMetricData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceMetricNodeList - errors', () => {
      it('should have a getDeviceMetricNodeList function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceMetricNodeList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceMetricNode - errors', () => {
      it('should have a getDeviceMetricNode function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceMetricNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getDeviceMetricNode(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getDeviceMetricNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPTPList - errors', () => {
      it('should have a getPTPList function', (done) => {
        try {
          assert.equal(true, typeof a.getPTPList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPTP - errors', () => {
      it('should have a getPTP function', (done) => {
        try {
          assert.equal(true, typeof a.getPTP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getPTP(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getPTP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editPTP - errors', () => {
      it('should have a editPTP function', (done) => {
        try {
          assert.equal(true, typeof a.editPTP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editPTP(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editPTP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editPTP('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editPTP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudCommunicationSettingsList - errors', () => {
      it('should have a getCloudCommunicationSettingsList function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudCommunicationSettingsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudCommunicationSettings - errors', () => {
      it('should have a getCloudCommunicationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudCommunicationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getCloudCommunicationSettings(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getCloudCommunicationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editCloudCommunicationSettings - errors', () => {
      it('should have a editCloudCommunicationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.editCloudCommunicationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editCloudCommunicationSettings(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editCloudCommunicationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editCloudCommunicationSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editCloudCommunicationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudManagementList - errors', () => {
      it('should have a getCloudManagementList function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudManagementList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addCloudManagement - errors', () => {
      it('should have a addCloudManagement function', (done) => {
        try {
          assert.equal(true, typeof a.addCloudManagement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addCloudManagement(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addCloudManagement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudManagement - errors', () => {
      it('should have a getCloudManagement function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudManagement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getCloudManagement(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getCloudManagement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editCloudManagement - errors', () => {
      it('should have a editCloudManagement function', (done) => {
        try {
          assert.equal(true, typeof a.editCloudManagement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editCloudManagement(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editCloudManagement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editCloudManagement('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editCloudManagement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCloudManagement - errors', () => {
      it('should have a deleteCloudManagement function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCloudManagement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteCloudManagement(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteCloudManagement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudServicesInfo - errors', () => {
      it('should have a getCloudServicesInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudServicesInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getCloudServicesInfo(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getCloudServicesInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudRegionList - errors', () => {
      it('should have a getCloudRegionList function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudRegionList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudRegion - errors', () => {
      it('should have a getCloudRegion function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudRegion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getCloudRegion(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getCloudRegion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addCloudEnrollmentImmediate - errors', () => {
      it('should have a addCloudEnrollmentImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.addCloudEnrollmentImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addCloudEnrollmentImmediate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addCloudEnrollmentImmediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addCloudUnenrollmentImmediate - errors', () => {
      it('should have a addCloudUnenrollmentImmediate function', (done) => {
        try {
          assert.equal(true, typeof a.addCloudUnenrollmentImmediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudEventsList - errors', () => {
      it('should have a getCloudEventsList function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudEventsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudEvents - errors', () => {
      it('should have a getCloudEvents function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getCloudEvents(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getCloudEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editCloudEvents - errors', () => {
      it('should have a editCloudEvents function', (done) => {
        try {
          assert.equal(true, typeof a.editCloudEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editCloudEvents(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editCloudEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editCloudEvents('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editCloudEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefenseOrchestratorList - errors', () => {
      it('should have a getDefenseOrchestratorList function', (done) => {
        try {
          assert.equal(true, typeof a.getDefenseOrchestratorList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefenseOrchestrator - errors', () => {
      it('should have a getDefenseOrchestrator function', (done) => {
        try {
          assert.equal(true, typeof a.getDefenseOrchestrator === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getDefenseOrchestrator(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getDefenseOrchestrator', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editDefenseOrchestrator - errors', () => {
      it('should have a editDefenseOrchestrator function', (done) => {
        try {
          assert.equal(true, typeof a.editDefenseOrchestrator === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editDefenseOrchestrator(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editDefenseOrchestrator', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editDefenseOrchestrator('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editDefenseOrchestrator', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSuccessNetworkList - errors', () => {
      it('should have a getSuccessNetworkList function', (done) => {
        try {
          assert.equal(true, typeof a.getSuccessNetworkList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSuccessNetwork - errors', () => {
      it('should have a getSuccessNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.getSuccessNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getSuccessNetwork(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSuccessNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editSuccessNetwork - errors', () => {
      it('should have a editSuccessNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.editSuccessNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editSuccessNetwork(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSuccessNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editSuccessNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSuccessNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSmartAgentConnectionList - errors', () => {
      it('should have a getSmartAgentConnectionList function', (done) => {
        try {
          assert.equal(true, typeof a.getSmartAgentConnectionList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSmartAgentConnection - errors', () => {
      it('should have a addSmartAgentConnection function', (done) => {
        try {
          assert.equal(true, typeof a.addSmartAgentConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSmartAgentConnection(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addSmartAgentConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSmartAgentConnection - errors', () => {
      it('should have a getSmartAgentConnection function', (done) => {
        try {
          assert.equal(true, typeof a.getSmartAgentConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getSmartAgentConnection(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSmartAgentConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editSmartAgentConnection - errors', () => {
      it('should have a editSmartAgentConnection function', (done) => {
        try {
          assert.equal(true, typeof a.editSmartAgentConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editSmartAgentConnection(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSmartAgentConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editSmartAgentConnection('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSmartAgentConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSmartAgentConnection - errors', () => {
      it('should have a deleteSmartAgentConnection function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSmartAgentConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteSmartAgentConnection(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteSmartAgentConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSmartAgentStatusList - errors', () => {
      it('should have a getSmartAgentStatusList function', (done) => {
        try {
          assert.equal(true, typeof a.getSmartAgentStatusList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSmartAgentStatus - errors', () => {
      it('should have a getSmartAgentStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getSmartAgentStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getSmartAgentStatus(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSmartAgentStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicenseList - errors', () => {
      it('should have a getLicenseList function', (done) => {
        try {
          assert.equal(true, typeof a.getLicenseList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addLicense - errors', () => {
      it('should have a addLicense function', (done) => {
        try {
          assert.equal(true, typeof a.addLicense === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addLicense(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addLicense', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicense - errors', () => {
      it('should have a getLicense function', (done) => {
        try {
          assert.equal(true, typeof a.getLicense === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getLicense(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getLicense', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLicense - errors', () => {
      it('should have a deleteLicense function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLicense === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteLicense(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteLicense', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSmartAgentSyncRequest - errors', () => {
      it('should have a addSmartAgentSyncRequest function', (done) => {
        try {
          assert.equal(true, typeof a.addSmartAgentSyncRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSmartAgentSyncRequest(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addSmartAgentSyncRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPLRRequestCode - errors', () => {
      it('should have a getPLRRequestCode function', (done) => {
        try {
          assert.equal(true, typeof a.getPLRRequestCode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getPLRRequestCode(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getPLRRequestCode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPLRRequestCodeList - errors', () => {
      it('should have a getPLRRequestCodeList function', (done) => {
        try {
          assert.equal(true, typeof a.getPLRRequestCodeList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPLRAuthorizationCode - errors', () => {
      it('should have a addPLRAuthorizationCode function', (done) => {
        try {
          assert.equal(true, typeof a.addPLRAuthorizationCode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addPLRAuthorizationCode(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addPLRAuthorizationCode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPLRReleaseCode - errors', () => {
      it('should have a addPLRReleaseCode function', (done) => {
        try {
          assert.equal(true, typeof a.addPLRReleaseCode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addPLRReleaseCode(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addPLRReleaseCode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkObjectList - errors', () => {
      it('should have a getNetworkObjectList function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkObjectList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addNetworkObject - errors', () => {
      it('should have a addNetworkObject function', (done) => {
        try {
          assert.equal(true, typeof a.addNetworkObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addNetworkObject(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addNetworkObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkObject - errors', () => {
      it('should have a getNetworkObject function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getNetworkObject(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getNetworkObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editNetworkObject - errors', () => {
      it('should have a editNetworkObject function', (done) => {
        try {
          assert.equal(true, typeof a.editNetworkObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editNetworkObject(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editNetworkObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editNetworkObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editNetworkObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkObject - errors', () => {
      it('should have a deleteNetworkObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteNetworkObject(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteNetworkObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkObjectGroupList - errors', () => {
      it('should have a getNetworkObjectGroupList function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkObjectGroupList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addNetworkObjectGroup - errors', () => {
      it('should have a addNetworkObjectGroup function', (done) => {
        try {
          assert.equal(true, typeof a.addNetworkObjectGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addNetworkObjectGroup(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addNetworkObjectGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkObjectGroup - errors', () => {
      it('should have a getNetworkObjectGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkObjectGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getNetworkObjectGroup(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getNetworkObjectGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editNetworkObjectGroup - errors', () => {
      it('should have a editNetworkObjectGroup function', (done) => {
        try {
          assert.equal(true, typeof a.editNetworkObjectGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editNetworkObjectGroup(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editNetworkObjectGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editNetworkObjectGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editNetworkObjectGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkObjectGroup - errors', () => {
      it('should have a deleteNetworkObjectGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkObjectGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteNetworkObjectGroup(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteNetworkObjectGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTCPPortObjectList - errors', () => {
      it('should have a getTCPPortObjectList function', (done) => {
        try {
          assert.equal(true, typeof a.getTCPPortObjectList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addTCPPortObject - errors', () => {
      it('should have a addTCPPortObject function', (done) => {
        try {
          assert.equal(true, typeof a.addTCPPortObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addTCPPortObject(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addTCPPortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTCPPortObject - errors', () => {
      it('should have a getTCPPortObject function', (done) => {
        try {
          assert.equal(true, typeof a.getTCPPortObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getTCPPortObject(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getTCPPortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editTCPPortObject - errors', () => {
      it('should have a editTCPPortObject function', (done) => {
        try {
          assert.equal(true, typeof a.editTCPPortObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editTCPPortObject(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editTCPPortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editTCPPortObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editTCPPortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTCPPortObject - errors', () => {
      it('should have a deleteTCPPortObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTCPPortObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteTCPPortObject(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteTCPPortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUDPPortObjectList - errors', () => {
      it('should have a getUDPPortObjectList function', (done) => {
        try {
          assert.equal(true, typeof a.getUDPPortObjectList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addUDPPortObject - errors', () => {
      it('should have a addUDPPortObject function', (done) => {
        try {
          assert.equal(true, typeof a.addUDPPortObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addUDPPortObject(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addUDPPortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUDPPortObject - errors', () => {
      it('should have a getUDPPortObject function', (done) => {
        try {
          assert.equal(true, typeof a.getUDPPortObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getUDPPortObject(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getUDPPortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editUDPPortObject - errors', () => {
      it('should have a editUDPPortObject function', (done) => {
        try {
          assert.equal(true, typeof a.editUDPPortObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editUDPPortObject(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editUDPPortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editUDPPortObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editUDPPortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUDPPortObject - errors', () => {
      it('should have a deleteUDPPortObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUDPPortObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteUDPPortObject(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteUDPPortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProtocolObjectList - errors', () => {
      it('should have a getProtocolObjectList function', (done) => {
        try {
          assert.equal(true, typeof a.getProtocolObjectList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addProtocolObject - errors', () => {
      it('should have a addProtocolObject function', (done) => {
        try {
          assert.equal(true, typeof a.addProtocolObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addProtocolObject(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addProtocolObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProtocolObject - errors', () => {
      it('should have a getProtocolObject function', (done) => {
        try {
          assert.equal(true, typeof a.getProtocolObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getProtocolObject(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getProtocolObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editProtocolObject - errors', () => {
      it('should have a editProtocolObject function', (done) => {
        try {
          assert.equal(true, typeof a.editProtocolObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editProtocolObject(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editProtocolObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editProtocolObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editProtocolObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteProtocolObject - errors', () => {
      it('should have a deleteProtocolObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteProtocolObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteProtocolObject(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteProtocolObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getICMPv4PortObjectList - errors', () => {
      it('should have a getICMPv4PortObjectList function', (done) => {
        try {
          assert.equal(true, typeof a.getICMPv4PortObjectList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addICMPv4PortObject - errors', () => {
      it('should have a addICMPv4PortObject function', (done) => {
        try {
          assert.equal(true, typeof a.addICMPv4PortObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addICMPv4PortObject(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addICMPv4PortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getICMPv4PortObject - errors', () => {
      it('should have a getICMPv4PortObject function', (done) => {
        try {
          assert.equal(true, typeof a.getICMPv4PortObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getICMPv4PortObject(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getICMPv4PortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editICMPv4PortObject - errors', () => {
      it('should have a editICMPv4PortObject function', (done) => {
        try {
          assert.equal(true, typeof a.editICMPv4PortObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editICMPv4PortObject(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editICMPv4PortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editICMPv4PortObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editICMPv4PortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteICMPv4PortObject - errors', () => {
      it('should have a deleteICMPv4PortObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteICMPv4PortObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteICMPv4PortObject(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteICMPv4PortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getICMPv6PortObjectList - errors', () => {
      it('should have a getICMPv6PortObjectList function', (done) => {
        try {
          assert.equal(true, typeof a.getICMPv6PortObjectList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addICMPv6PortObject - errors', () => {
      it('should have a addICMPv6PortObject function', (done) => {
        try {
          assert.equal(true, typeof a.addICMPv6PortObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addICMPv6PortObject(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addICMPv6PortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getICMPv6PortObject - errors', () => {
      it('should have a getICMPv6PortObject function', (done) => {
        try {
          assert.equal(true, typeof a.getICMPv6PortObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getICMPv6PortObject(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getICMPv6PortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editICMPv6PortObject - errors', () => {
      it('should have a editICMPv6PortObject function', (done) => {
        try {
          assert.equal(true, typeof a.editICMPv6PortObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editICMPv6PortObject(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editICMPv6PortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editICMPv6PortObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editICMPv6PortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteICMPv6PortObject - errors', () => {
      it('should have a deleteICMPv6PortObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteICMPv6PortObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteICMPv6PortObject(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteICMPv6PortObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortObjectGroupList - errors', () => {
      it('should have a getPortObjectGroupList function', (done) => {
        try {
          assert.equal(true, typeof a.getPortObjectGroupList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPortObjectGroup - errors', () => {
      it('should have a addPortObjectGroup function', (done) => {
        try {
          assert.equal(true, typeof a.addPortObjectGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addPortObjectGroup(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addPortObjectGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortObjectGroup - errors', () => {
      it('should have a getPortObjectGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getPortObjectGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getPortObjectGroup(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getPortObjectGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editPortObjectGroup - errors', () => {
      it('should have a editPortObjectGroup function', (done) => {
        try {
          assert.equal(true, typeof a.editPortObjectGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editPortObjectGroup(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editPortObjectGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editPortObjectGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editPortObjectGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePortObjectGroup - errors', () => {
      it('should have a deletePortObjectGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deletePortObjectGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deletePortObjectGroup(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deletePortObjectGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFeatureCapabilities - errors', () => {
      it('should have a getFeatureCapabilities function', (done) => {
        try {
          assert.equal(true, typeof a.getFeatureCapabilities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getFeatureCapabilities(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getFeatureCapabilities', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHTTPProxyList - errors', () => {
      it('should have a getHTTPProxyList function', (done) => {
        try {
          assert.equal(true, typeof a.getHTTPProxyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHTTPProxy - errors', () => {
      it('should have a getHTTPProxy function', (done) => {
        try {
          assert.equal(true, typeof a.getHTTPProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getHTTPProxy(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getHTTPProxy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editHTTPProxy - errors', () => {
      it('should have a editHTTPProxy function', (done) => {
        try {
          assert.equal(true, typeof a.editHTTPProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editHTTPProxy(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editHTTPProxy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editHTTPProxy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editHTTPProxy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretList - errors', () => {
      it('should have a getSecretList function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSecret - errors', () => {
      it('should have a addSecret function', (done) => {
        try {
          assert.equal(true, typeof a.addSecret === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSecret(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addSecret', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecret - errors', () => {
      it('should have a getSecret function', (done) => {
        try {
          assert.equal(true, typeof a.getSecret === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getSecret(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getSecret', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editSecret - errors', () => {
      it('should have a editSecret function', (done) => {
        try {
          assert.equal(true, typeof a.editSecret === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editSecret(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSecret', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editSecret('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editSecret', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecret - errors', () => {
      it('should have a deleteSecret function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecret === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteSecret(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteSecret', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTimeRangeObjectList - errors', () => {
      it('should have a getTimeRangeObjectList function', (done) => {
        try {
          assert.equal(true, typeof a.getTimeRangeObjectList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addTimeRangeObject - errors', () => {
      it('should have a addTimeRangeObject function', (done) => {
        try {
          assert.equal(true, typeof a.addTimeRangeObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addTimeRangeObject(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addTimeRangeObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTimeRangeObject - errors', () => {
      it('should have a getTimeRangeObject function', (done) => {
        try {
          assert.equal(true, typeof a.getTimeRangeObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getTimeRangeObject(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getTimeRangeObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editTimeRangeObject - errors', () => {
      it('should have a editTimeRangeObject function', (done) => {
        try {
          assert.equal(true, typeof a.editTimeRangeObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editTimeRangeObject(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editTimeRangeObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editTimeRangeObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editTimeRangeObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTimeRangeObject - errors', () => {
      it('should have a deleteTimeRangeObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTimeRangeObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteTimeRangeObject(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteTimeRangeObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTimeZoneSettingList - errors', () => {
      it('should have a getTimeZoneSettingList function', (done) => {
        try {
          assert.equal(true, typeof a.getTimeZoneSettingList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTimeZoneSetting - errors', () => {
      it('should have a getTimeZoneSetting function', (done) => {
        try {
          assert.equal(true, typeof a.getTimeZoneSetting === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getTimeZoneSetting(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getTimeZoneSetting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editTimeZoneSetting - errors', () => {
      it('should have a editTimeZoneSetting function', (done) => {
        try {
          assert.equal(true, typeof a.editTimeZoneSetting === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editTimeZoneSetting(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editTimeZoneSetting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editTimeZoneSetting('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editTimeZoneSetting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTimeZoneObjectList - errors', () => {
      it('should have a getTimeZoneObjectList function', (done) => {
        try {
          assert.equal(true, typeof a.getTimeZoneObjectList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addTimeZoneObject - errors', () => {
      it('should have a addTimeZoneObject function', (done) => {
        try {
          assert.equal(true, typeof a.addTimeZoneObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addTimeZoneObject(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-addTimeZoneObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTimeZoneObject - errors', () => {
      it('should have a getTimeZoneObject function', (done) => {
        try {
          assert.equal(true, typeof a.getTimeZoneObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getTimeZoneObject(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getTimeZoneObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editTimeZoneObject - errors', () => {
      it('should have a editTimeZoneObject function', (done) => {
        try {
          assert.equal(true, typeof a.editTimeZoneObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.editTimeZoneObject(null, null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editTimeZoneObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editTimeZoneObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-editTimeZoneObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTimeZoneObject - errors', () => {
      it('should have a deleteTimeZoneObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTimeZoneObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.deleteTimeZoneObject(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-deleteTimeZoneObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTimeZones - errors', () => {
      it('should have a getTimeZones function', (done) => {
        try {
          assert.equal(true, typeof a.getTimeZones === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objId', (done) => {
        try {
          a.getTimeZones(null, (data, error) => {
            try {
              const displayE = 'objId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-getTimeZones', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#token - errors', () => {
      it('should have a token function', (done) => {
        try {
          assert.equal(true, typeof a.token === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.token(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_firepowerthreatdefense-adapter-token', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiVersions - errors', () => {
      it('should have a getApiVersions function', (done) => {
        try {
          assert.equal(true, typeof a.getApiVersions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
