# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Firepower_threat_defense System. The API that was used to build the adapter for Firepower_threat_defense is usually available in the report directory of this adapter. The adapter utilizes the Firepower_threat_defense API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Cisco Firepower Threat Defense adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Cisco Firepower Threat Defense to offer security and trust information. 

With this adapter you have the ability to perform operations with Cisco Firepower Threat Defense such as:

- Objects
- Policies
- Devices
- DNS
- Jobs

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
